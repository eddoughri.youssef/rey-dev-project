import React from 'react'

import style from './compliance.module.css';


import concern1 from "../media/concern1_security.png";
import concern2 from "../media/concern2_organization.png";
import concern3 from "../media/concern3_virus.png";

function Card({img, title, desc}){
  return (
    <div className={style.card}>
      <div className={style.img}>
        <img src={img} alt='describing the concern'/>
      </div>
      <div className={style.title}>
        {title}
      </div>
      <div className='font-poppins text-center leading-5 max-w-[80%] font-light'>
        {desc}
      </div>
    </div>
  )
}



function Compliance() {
  const dumyText = "In the first two weeks of June alone, hackers have compromised dozens of Discord servers. Among the hacked Discords w";
  return (
    <section data-aos="fade-up" data-aos-duration="4000" className={style.compliance}>
        <header>
          <h1>Challenges</h1>
        </header>
        <div className={style.cards+ " flex-col md:flex-row"}>
          <Card img={concern1} title={"Security"} desc={dumyText}/>
          <Card img={concern2} title={"Scams"} desc={dumyText}/>
          <Card img={concern3} title={"Lack of organisation"} desc={dumyText}/>
        </div>
    </section>
  )
}

export default Compliance;