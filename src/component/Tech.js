import React, { useState } from 'react'
import VideoCard from './alien tech/VideoCard'
import PlayCircleOutlinedIcon from '@mui/icons-material/PlayCircleOutlined'

import style from "./tech.module.css";

function AlienTech() {

    const [video, changeVideo] = useState(1);

    return (
        <section data-aos="fade-up" data-aos-duration="4000" className={style.tech} id="features">
            <header>
                <h1>FANCY FEATURES</h1>
                <p className='font-poppins font-light text-lg text-center text-white'>
                Supercharging our bots with a wide range of features to enhance your Discord experience.
                </p>
            </header>
            {/* <h1 className='font-bebasNeue text-5xl sm:text-header text-bg-blue text-center'>Alien Tech</h1>

            <div className='font-poppins text-bg-blue text-paragraph text-center'>
                <p className=''>We're announcing a new type of community designed to reward</p>
                <p className=''>public goods and build a sustainable future for Ethereum.</p>
            </div> */}

            <div className="flex flex-col lg:flex-row justify-center items-center w-screen overflow-hidden">
                <div className='flex flex-row w-screen justify-evenly lg:w-auto lg:flex-col'>
                    <button onClick={() => changeVideo(1)} className={`font-poppins text-sm drop-shadow-skyblue rounded-lg py-2 flex-auto lg:px-16 lg:mt-5 md:mr-6   ${video === 1 ? 'bg-bg-skyBlue text-white text-white' : 'bg-[#282828] dark:bg-darkbg-card text-bg-blue dark:text-white'}`}>
                        <h1 className=''><span className={`${video === 1 ? 'text-white' : 'text-bg-skyBlue'}`}><PlayCircleOutlinedIcon /></span>Security tools</h1>
                    </button>

                    <button onClick={() => changeVideo(2)} className={`font-poppins text-sm drop-shadow-skyblue rounded-lg py-2 flex-auto lg:px-16 lg:mt-5 md:mr-6 ${video === 2 ? 'bg-bg-skyBlue text-white' : 'bg-[#282828] dark:bg-darkbg-card text-bg-blue dark:text-white'}`}>
                        <h1 className=''><span className={`${video === 2 ? 'text-white' : 'text-bg-skyBlue'}`}><PlayCircleOutlinedIcon /></span>Tl;dr</h1>
                    </button>

                    <button onClick={() => changeVideo(3)} className={`font-poppins text-sm drop-shadow-skyblue rounded-lg py-2 flex-auto lg:px-16 lg:mt-5 md:mr-6 ${video === 3 ? 'bg-bg-skyBlue text-white' : 'bg-[#282828] dark:bg-darkbg-card text-bg-blue dark:text-white'}`}>
                        <h1 className=''><span className={`${video === 3 ? 'text-white' : 'text-bg-skyBlue'}`}><PlayCircleOutlinedIcon /></span>Fancy UI</h1>
                    </button>

                    <button onClick={() => changeVideo(4)} className={`font-poppins text-sm drop-shadow-skyblue rounded-lg py-2 flex-auto lg:px-16 lg:mt-5 md:mr-6 ${video === 4 ? 'bg-bg-skyBlue text-white' : 'bg-[#282828] dark:bg-darkbg-card text-bg-blue dark:text-white'}`}>
                        <h1 className=''><span className={`${video === 4 ? 'text-white' : 'text-bg-skyBlue'}`}><PlayCircleOutlinedIcon /></span>Smart FAQ</h1>
                    </button>
                </div>
                <VideoCard video={`https://jisa-web-app-media.s3.eu-west-3.amazonaws.com/video${video}.mp4`} />

                <div className='flex flex-row w-screen justify-evenly lg:w-auto lg:flex-col'>

                    <button onClick={() => changeVideo(5)} className={`font-poppins text-sm drop-shadow-skyblue rounded-lg py-2 flex-auto lg:px-16 lg:mt-5 md:ml-6 ${video === 5 ? 'bg-bg-skyBlue text-white' : 'bg-[#282828] dark:bg-darkbg-card text-bg-blue dark:text-white'}`}>
                        <h1 className=''><span className={`${video === 5 ? 'text-white' : 'text-bg-skyBlue'}`}><PlayCircleOutlinedIcon /></span>Explain to a 10 yo</h1>
                    </button>

                    <button onClick={() => changeVideo(6)} className={`font-poppins text-sm drop-shadow-skyblue rounded-lg py-2 flex-auto lg:px-16 lg:mt-5 md:ml-6 ${video === 6 ? 'bg-bg-skyBlue text-white' : 'bg-[#282828] dark:bg-darkbg-card text-bg-blue dark:text-white'}`}>
                        <h1 className=''><span className={`${video === 6 ? 'text-white' : 'text-bg-skyBlue'}`}><PlayCircleOutlinedIcon /></span>Auto-Answer</h1>
                    </button>

                    <button onClick={() => changeVideo(7)} className={`font-poppins text-sm drop-shadow-skyblue rounded-lg py-2 flex-auto lg:px-16 lg:mt-5 md:ml-6 ${video === 7 ? 'bg-bg-skyBlue text-white' : 'bg-[#282828] dark:bg-darkbg-card text-bg-blue dark:text-white'}`}>
                        <h1 className=''><span className={`${video === 7 ? 'text-white' : 'text-bg-skyBlue'}`}><PlayCircleOutlinedIcon /></span>ML Translation</h1>
                    </button>

                    <button onClick={() => changeVideo(8)} className={`font-poppins text-sm drop-shadow-skyblue rounded-lg py-2 flex-auto lg:px-16 lg:mt-5 md:ml-6 ${video === 8 ? 'bg-bg-skyBlue text-white' : 'bg-[#282828] dark:bg-darkbg-card text-bg-blue dark:text-white'}`}>
                        <h1 className=''><span className={`${video === 8 ? 'text-white' : 'text-bg-skyBlue'}`}><PlayCircleOutlinedIcon /></span>Ready Templates</h1>
                    </button>
                </div>
            </div>

            {/* <div className="flex justify-center items-center font-poppins mt-6">
                <h1 className='font-bold text-2xl sm:text-3xl text-bg-blue mr-5 sm:mr-6'>Ready?</h1>
                <button className='bg-bg-skyBlue rounded-full font-bold text-white text-xs py-3 px-7 my-3'>Order Jisa</button>
            </div> */}
        </section>
    )
}

export default AlienTech;