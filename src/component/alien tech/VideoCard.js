import React from 'react'
import ReactPlayer from 'react-player'

function VideoCard({video}) {
  return (
    <div className='drop-shadow-blue w-[95vw] h-[50vh] md:w-[50vw]  rounded-2xl mt-8 mb-8 lg:mb-0 overflow-hidden'>
        <ReactPlayer
            url={video}
            playing={true} muted={true} controls={true} loop={true} width='100%' height='100%'
          />
    </div>
  )
}

export default VideoCard