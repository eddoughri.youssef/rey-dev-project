import React, {useContext} from 'react';

import { Box } from '@mui/system';
import { Typography } from '@mui/material';

import jisaLogo from '../../media/jisa_lg.png';

import { PaymentInfoContext } from '../../pages/payment';


function DescribtionCard({step}) {
  const [paymentInfo, pinfoContext] = useContext(PaymentInfoContext);
  // const imgdisplayed = paymentInfo.Personal === null ? jisaLogo : paymentInfo.Personal.avatarURL;
  // const imgdisplayed = paymentInfo.Personal === null ? jisaLogo : paymentInfo.Personal.avatarURL;
  return (
    <Box
      sx={{
        width: "80%",
        display: 'flex',
        flexDirection: 'column',
        justifyContent: "center",
        alignItems: "center",
        gap: "20px",
      }}
    >
      <Box
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}
        >
        <Box
          sx={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            height: "170px",
            width: "170px",
            borderRadius: "100%",
            backgroundColor: "black",
            marginDown: "11px",
            overflow: "hidden",
            ...(((paymentInfo.Personal !== null && paymentInfo.Personal.avatarUrl && step === 0) ) && {'& img': {
              height: "140%",
              width: "100%",
              objectFit: "cover",
            }})
          }}>
          <img src={(step === 0 ? (paymentInfo.Personal !== null && paymentInfo.Personal.avatarUrl) ? paymentInfo.Personal.avatarUrl: jisaLogo : jisaLogo)} alt="jisa logo"/>
        </Box>
        <Typography
          sx={{
            fontFamily: "Poppins",
            fontWeight: "600",
            fontSize: "1.9rem",
          }}
        >{step === 0 ? 
              (paymentInfo.Personal === null ? "Jisa" : paymentInfo.Personal.username) : 
              (paymentInfo.Plan.plan === "Pro" ? 
                          (paymentInfo.Plan.botName !== "" ? paymentInfo.Plan.botName : "Bot~Name") :
                          "Jisa")}</Typography>
        <Typography sx={{
          fontFamily: "Poppins",
          fontSize: "small",
          fontStyle: "italic",
          fontWeight: "300",
          textAlign: "center",
          opacity: "0.6",            
        }}>{paymentInfo.Plan.plan}</Typography>
      </Box>
      <Box
        sx={{
          maxWidth: "85%",
        }}
        >
        <Typography
          sx={{
            fontFamily: "Poppins",
            fontWeight: "300",
            textAlign: "center",
            opacity: "0.6",
          }}>
          {"Korem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, ctum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan,"}
        </Typography>
      </Box>
    </Box>
  )
}

export default DescribtionCard