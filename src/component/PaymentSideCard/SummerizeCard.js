import React from 'react'

import { Box } from '@mui/system'
import Typography from '@mui/material/Typography'

import checkIcon from "../../media/check.svg"
function SummerizeCard() {
  return (
    <Box
        sx={{
            width: "80%",
            height: "90%",
            display: 'flex',
            flexDirection: 'column',

            justifyContent: "center",
            // alignItems: "center",

            // display: 'grid',

            // gap: "10%",
        }}
    >
        <Box
            sx={{
            display: "flex",
            justifyContent: "flex-start",
            // alignItems: "center",
            flexDirection: "column",
            }}
        >
            <Typography
                sx={{
                    fontSize: "1.2rem",
                    fontFamily: "Poppins",
                    fontWeight: "500",
                    opacity: "0.6",
                }}
            >{"You've to pay."}</Typography>
            <Typography sx={{
                fontFamily: "Courier New",
                // fontFamily: "Courier Prime",
                fontSize: "4.5rem",
                fontWeight: "800",
                letterSpacing: "0.2rem",
                "&>span": {
                    fontSize: "50%",
                    fontWeight: "500",
                    opacity: "0.6",
                }
            }}>{"$300"}<span>{".00"}</span></Typography>
        </Box>
        <Box
            sx={{
                maxWidth: "85%",
                display: "flex",
                flexDirection: "column",
                gap: "20px",
            }}
        >
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "5px",    
                }}
            >
                <Box
                    sx={{
                        display: "flex",
                        justifyContent: "flex-start",
                        gap: "10px",    
                    }}
                >
                    <Box
                        sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            backgroundColor: "green",
                            width: "30px",
                            height: "30px",
                            borderRadius: "15px",
                        }}
                    >
                        <img src={checkIcon} alt="check icon" />
                    </Box>
                    <Typography
                        sx={{
                            fontFamily: "Poppins",
                            fontWeight: "600",
                            fontSize: "1.5rem",
                        }}
                    >{"Payment & Invoice"}</Typography>
                </Box>
                <Typography
                    sx={{
                        fontFamily: "Poppins",
                        fontWeight: "300",
                        opacity: "0.6",
                    }}>
                    {"Korem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, ctum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan,"}
                </Typography>
            </Box>
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "5px",    
                }}
            >
                <Box
                    sx={{
                        display: "flex",
                        justifyContent: "flex-start",
                        gap: "10px",    
                    }}
                >
                    <Box
                        sx={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            backgroundColor: "green",
                            width: "30px",
                            height: "30px",
                            borderRadius: "15px",
                            
                        }}
                    >
                        <img src={checkIcon} alt="check icon" />
                    </Box>
                    <Typography
                        sx={{
                            fontFamily: "Poppins",
                            fontWeight: "600",
                            fontSize: "1.5rem",
                        }}
                    >{"Discounts & Offers"}</Typography>
                </Box>
                <Typography
                    sx={{
                        fontFamily: "Poppins",
                        fontWeight: "300",
                        opacity: "0.6",
                    }}>
                    {"Korem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, ctum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan,"}
                </Typography>
            </Box>
        </Box>
    </Box>
  )
}

export default SummerizeCard;