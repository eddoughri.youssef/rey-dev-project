import React, { useState } from "react";

import { Box } from "@mui/system";
import Input from "@mui/material/Input";
import Typography from "@mui/material/Typography";
import FormControl from "@mui/material/FormControl";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";

import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";

import masterCardIcon from "../../media/masterCardIcon.png";
function PurchaseForm() {
  const [selectedDate, setSelectedDate] = useState(null);
  const handleDateChange = (date) => {
    setSelectedDate(date);
  };

  const [cvc, setCvc] = React.useState("");
  const onCvcChange = (event) => {
    const {
      target: { value },
    } = event;
    if (value.match(/^[0-9]{0,4}$/)) setCvc(value);
  };

  const [cardNumber, setCardNumber] = React.useState("");
  const onCardNumberChange = (event) => {
    const unfold = (value) => {
      let result = "";
      for (let i = 0; i < value.length; i++) {
        if (i % 4 === 0 && i !== 0) result += " ";
        result += value[i];
      }
      return result;
    };
    const fold = (value) => {
      let result = "";
      for (let i = 0; i < value.length; i++) {
        if (value[i] !== " ") result += value[i];
      }
      return result;
    };

    const {
      target: { value },
    } = event;
    const foldedvalue = fold(value);
    if (foldedvalue.match(/^[0-9]{0,16}$/)) {
      setCardNumber(unfold(foldedvalue));
    }
  };
  return (
    <Box
      sx={{
        flex: "10 1 auto",
        display: "flex",
        flexDirection: "column",
        justifyContent: "start",
        gap: "5%",
        // marginTop: "1%",
      }}
    >
      <Box
        sx={{
          display: "flex",
          gap: "20px",
          alignItems: "stretch",
        }}
      >
        <Box sx={{ flex: "1 1 auto" }}>
          <Typography
            sx={{
              fontFamily: "Poppins",
              marginLeft: ".75rem",
              marginBottom: ".5rem",
            }}
            htmlFor="Email"
          >
            Email
          </Typography>
          <Input
            // hiddenLabel
            placeholder="testmail@mail.com"
            id="Email"
            type="email"
            sx={{
              backgroundColor: "#2C2C2C",
              color: "white",
              padding: "12px 10px 12px",
              width: "100%",
              fontFamily: 'fira code, monospace',
              borderRadius: '12px',
              '&:focus-within':{
                borderRadius: '12px 12px 0 0'
              }
            }}
          />
        </Box>
        <Box sx={{ flex: "1 1 auto" }}>
          <Typography
            sx={{
              fontFamily: "Poppins",
              marginLeft: ".75rem",
              marginBottom: ".5rem",
            }}
            htmlFor="Name"
          >
            Name on Card
          </Typography>
          <Input
            // hiddenLabel
            placeholder="Jhon smith"
            id="Name"
            type="text"
            sx={{
              backgroundColor: "#2C2C2C",
              color: "white",
              padding: "12px 10px 12px",
              width: "100%",
              fontFamily: 'fira code, monospace',
              borderRadius: '12px',
              '&:focus-within':{
                borderRadius: '12px 12px 0 0'
              }
            }}
          />
        </Box>
      </Box>
      <Box>
        <Typography
          sx={{
            fontFamily: "Poppins",
            marginLeft: ".75rem",
            marginBottom: ".5rem",
          }}
          htmlFor="Card"
        >
          Card Number
        </Typography>
        <FormControl fullWidth sx={{ m: 1 }}>
          <Input
            // hiddenLabel
            value={cardNumber}
            onChange={onCardNumberChange}
            sx={{
              backgroundColor: "#2C2C2C",
              color: "white",
              padding: "12px 10px 12px 12px",
              width: "99%",
              fontFamily: 'fira code, monospace',
              borderRadius: '12px',
              '&:focus-within':{
                borderRadius: '12px 12px 0 0'
              }

            }}
            startAdornment={
              <InputAdornment position="start">
                <img src={masterCardIcon} alt="master card logo" />
              </InputAdornment>
            }
          />
        </FormControl>
      </Box>
      <Box
        sx={{
          display: "flex",
          gap: "20px",
          alignItems: "stretch",
        }}
      >
        <Box sx={{ flex: "1 1 auto" }}>
          <Typography
            sx={{
              fontFamily: "Poppins",
              marginLeft: ".75rem",
              marginBottom: ".5rem",
            }}
            htmlFor="Expiry"
          >
            Expiry
          </Typography>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              // label="Select date"
              value={selectedDate}
              onChange={handleDateChange}
              views={["year", "month"]}
              minDate={new Date()}
              renderInput={(params) => (
                <TextField
                  sx={{
                    backgroundColor: "#2C2C2C",
                    color: "white",
                    fontFamily: 'fira code, monospace',
                    "& *": { color: "white" },
                    borderRadius: '12px',
                    '&:focus-within':{
                      borderRadius: '12px 12px 0 0'
                    }
                  }}
                  {...params}
                />
              )}
            />
          </LocalizationProvider>
        </Box>
        <Box sx={{ flex: "1 1 auto" }}>
          <Typography
            sx={{
              fontFamily: "Poppins",
              marginLeft: ".75rem",
              marginBottom: ".5rem",
            }}
            htmlFor="CVC"
          >
            CVC / CVV
          </Typography>
          <Input
            // hiddenLabel
            // label="CVC"
            variant="filled"
            type="tel"
            value={cvc}
            onChange={onCvcChange}
            placeholder="X X X (X)"
            inputProps={{
              maxLength: 4,
              pattern: "[0-9]{3,4}",
              autoComplete: "cc-csc",
              inputMode: "numeric",
            }}
            sx={{
              backgroundColor: "#2C2C2C",
              color: "white",
              fontFamily: 'fira code, monospace',
              padding: "12px 10px 12px",
              borderRadius: '12px',
              '&:focus-within':{
                borderRadius: '12px 12px 0 0'
              }
            }}
          />
        </Box>
      </Box>
    </Box>
  );
}

export default PurchaseForm;
