import React from "react";

import PaymentHeader from "../PaymentHeader";

function PaymentHeaderForm() {
  const title = "Second step, give us more info ";
  const text =
    "To finalize your subscription, kindly complete your payment using a valid credit card.";
  return PaymentHeader({ title, text });
}

export default PaymentHeaderForm;
