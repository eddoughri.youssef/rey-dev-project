import React, { useEffect, useContext, useState } from "react";

import { Box } from "@mui/system";
import Input from "@mui/material/Input";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Typography from "@mui/material/Typography";
import FilledInput from "@mui/material/FilledInput";
import Button from "@mui/material/Button";

import uploadIcon from "../../media/upload_icon.png";

import {PaymentInfoContext} from "../../pages/payment";


function PlanForm() {
  const plans = [
    "Pro",
    "Commun"
  ];
  const [paymentInfo, pinfoContext] = useContext(PaymentInfoContext);
  const [chosenPlan, setChosenPlan] = useState("Pro");
  const [botName, setBotName] = useState("");
  const handleChange = (event) => {
    const {
      target: { value },
    } = event;
    setChosenPlan(value);
    const payload = value;
    pinfoContext({type: "UpdatePlan", payload});
  };

  useEffect(() => {
    if(paymentInfo.Personal === null) return;
    setChosenPlan(paymentInfo.Plan.plan);
    setBotName(paymentInfo.Plan.botName);
    console.log(paymentInfo);
  }, []);


  const StandarePlan = (
    <Box
      sx={{
        flex: "10 1 auto",
        display: "flex",
        flexDirection: "column",
        justifyContent: "start",
        gap: "15%",
        marginTop: "5%",
      }}
    >
      <Box
        sx={{
          display: "flex",
          // justifyContent: "stretch",
          gap: "20px",
          alignItems: "stretch",
          
        }}
      >
        <Box
          sx={{flex: "1 1 auto"}}
        >
          <Typography
            sx={{
              fontFamily: "Poppins",
              marginLeft: ".75rem",
              marginBottom: ".5rem"
            }}
          >
            Bot name
          </Typography>
          <Input
            placeholder="Bot Name"
            disabled={chosenPlan === "Commun"}
            onChange={(e) =>{
              const payload = e.target.value;
              if(payload.length > 18) return;
              setBotName(payload);
              pinfoContext({type: "UpdateBotName", payload});
            }}
            value={botName}
            sx={{
              backgroundColor: '#2C2C2C',
              color: 'white',
              padding: '12px 10px 12px',
              width: '100%',
              fontFamily: 'fira code, monospace',
              borderRadius: '12px',
              '&:focus-within':{
                borderRadius: '12px 12px 0 0'
              }
            }}
          />
        </Box>
        <Box
          sx={{flex: "1 1 auto"}}
        >
          <Typography
              sx={{
                fontFamily: "Poppins",
                marginLeft: ".75rem",
                marginBottom: ".5rem"
              }}
            >
              Bot Image
          </Typography>
          <Button 
            variant="contained"
            component="label"
            disabled={chosenPlan === "Commun"}
            sx={{
              backgroundColor: '#2C2C2C',
              color: 'white',
              padding: '16px 10px 16px',
              width: '100%',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-evenly',
              fontFamily: 'fira code, monospace',
              borderRadius: '12px',
            }}
            >
              <img src={uploadIcon} alt='upload button'/>
              <span>Upload a picture</span>
            <input hidden accept="image/*" multiple type="file" />
          </Button>
        </Box>
      </Box>
      <Box>
        <Typography
          sx={{
            fontFamily: "Poppins",
            marginLeft: ".75rem",
            marginBottom: ".5rem",
          }}
        >
          Pick a package
        </Typography>
        <Select
          value={chosenPlan}
          onChange={handleChange}
          input={<FilledInput label="Plan" />}
          // disabled
          sx={{
            color: 'white',
            backgroundColor: '#2C2C2C',
            minWidth: '100%',
            fontFamily: 'fira code, monospace',
            '&:focus-within, &:hover':{
              backgroundColor: '#4C4C4C',
            },
            '&>*':{
              backgroundColor: '#2C2C2C',
              padding: '12px',
              borderRadius: '12px',
              '&:focus-within':{
                borderRadius: '12px 12px 0 0'
              }
            }
          }}
          >
            {plans.map((plan, index)=>{
              return <MenuItem value={plan} key={index+plan}>{plan}</MenuItem>
            })}
        </Select>
      </Box>
    </Box>
  );
  const ChangePlan = (
    <Box
      sx={{
        flex: "10 1 auto",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Button 
        variant="contained"
        sx={{
          background: 'linear-gradient(90deg, #41ABF3, #3C19B9)',
          color: 'white',
          padding: '16px 10px 16px',
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-evenly',
          fontFamily: 'fira code, monospace',
          borderRadius: '12px',
        }}>
          Change Plan
      </Button>
    </Box>
  );
  return paymentInfo.Subscribtion.status ? ChangePlan : StandarePlan;
}

export default PlanForm;