import React from 'react'
import DoneIcon from '@mui/icons-material/Done'
import CloseIcon from '@mui/icons-material/Close'

import style from "../pricing.module.css";

function CommunPrice() {
    return (
        <div id={style.communPrice} className='bg-[#282828] flex flex-col justify-between items-center bg-dark w-64 sm:w-72 h-72 sm:h-[300px] rounded-3xl drop-shadow-skyblue my-5 mr-0 sm:mr-10 overflow-hidden'>
            <div className='w-full bg-bg-gray/50 dark:bg-darkbg-gray/50 border-bg-blue border-b-2 dark:border-none rounded-t-2xl'>
                <h1 className='font-poppins text-xl text-bg-blue dark:text-white font-bold text-center my-3'>Commun</h1>
            </div>

            <div className='mt-4'>
                <h1 className='font-poppins text-bg-blue dark:text-white text-paragraph'><DoneIcon color="success" className='mr-3' /> Unlike every other bo</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-paragraph'><DoneIcon color="success" className='mr-3' /> Implemented double s</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-paragraph'><DoneIcon color="success" className='mr-3' /> Securi priority when deve</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-paragraph'><span className='text-bg-red'><CloseIcon className='mr-3' /></span> Unlike every other bot w</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-paragraph'><span className='text-bg-red'><CloseIcon className='mr-3' /></span> Securi priority when deve</h1>
            </div>

            <div className='flex justify-center items-center font-poppins mt-1'>
                <h1 className=' text-bg-blue  dark:text-white font-bold text-2xl mr-1'>30<span className='text-lg'>$</span></h1>
                <h1 className='text-gray-300 font-normal text-sm'> /MO</h1>
            </div>

            <div className={style.ocean}>
                <div className={style.wave} />
                <div className={style.wave} />
                <div className={style.wave} />
            </div>
            {/* <img width="288px" src={SkyBlue} alt="" className='rounded-b-3xl' /> */}
        </div>
    )
}

export default CommunPrice