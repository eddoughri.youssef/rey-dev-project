import React from 'react'
import DoneIcon from '@mui/icons-material/Done'

import style from "../pricing.module.css";


function EntreprisePrice() {
  return (
    <div id={style.entreprisePrice} className='bg-[#282828] flex flex-col justify-between items-center bg-dark w-64 sm:w-72 h-72 sm:h-[300px] rounded-3xl drop-shadow-skyblue my-5 ml-0 sm:ml-10 overflow-hidden'>
            <div className='w-full bg-bg-gray/50 dark:bg-darkbg-gray/50 border-bg-blue border-b-2 dark:border-none rounded-t-2xl'>
                <h1 className='font-poppins text-xl text-bg-blue dark:text-white font-bold text-center my-3'>Entreprise</h1>
            </div>

            <div className='mt-4'>
                <h1 className='font-poppins text-bg-blue dark:text-white text-paragraph'><DoneIcon color="success" className='mr-3' /> Unlike every other bo</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-paragraph'><DoneIcon color="success" className='mr-3' /> Implemented double s</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-paragraph'><DoneIcon color="success" className='mr-3' /> Securi priority when deve</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-paragraph'><DoneIcon color="success" className='mr-3' /> Unlike every other bot w</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-paragraph'><DoneIcon color="success" className='mr-3' /> Securi priority when deve</h1>
            </div>

            {/* <div className='flex justify-center items-center font-poppins mt-1'>
                <h1 className=' text-bg-blue font-bold text-2xl mr-1'>30<span className='text-lg'>$</span></h1>
                <h1 className='text-gray-300 font-normal text-sm'> /MO</h1>
            </div> */}
            <button className='bg-bg-skyBlue rounded-full font-poppins font-bold text-white text-[11px] py-2.5 px-5 mt-1'>Contact us</button>

            <div className={style.ocean}>
                <div className={style.wave} />
                <div className={style.wave} />
                <div className={style.wave} />
            </div>
            {/* <img width="288px" src={Green} alt="" className='rounded-b-3xl' /> */}
        </div>
  )
}

export default EntreprisePrice