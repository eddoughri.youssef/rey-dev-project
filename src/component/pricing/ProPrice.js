import React from 'react'
import DoneIcon from '@mui/icons-material/Done'
import CloseIcon from '@mui/icons-material/Close'

import style from "../pricing.module.css";


function ProPrice() {
    return (
        <div id={style.proPrice} className='bg-[#282828] flex flex-col justify-between items-center bg-dark w-72 sm:w-80 h-[300px] sm:h-[340px] rounded-3xl drop-shadow-skyblue my-5 overflow-hidden'>
            <div className='w-full bg-bg-gray/50 dark:bg-darkbg-gray/50 border-bg-blue border-b-2 dark:border-none rounded-t-2xl'>
                <h1 className='font-poppins text-2xl text-bg-blue dark:text-white font-bold text-center my-3'>Pro</h1>
            </div>
            {/* <div class="bg-bg-skyBlue origin-top float-right mt-7 mr-2 lg:mr-12 w-72 lg:w-[300px] xl:w-[360px] text-center py-2" style="transform: translateX(49%) rotate(45deg);"><h1 class="font-poppins font-bold text-white text-xs lg:text-sm mx-auto my-1 select-none">Best Value</h1></div> */}
            <div className="absolute top-4 left-full rotate-45 bg-bg-skyBlue px-3 py-2"
                    style={{transform: "translateX(-70%) translateY(-5%) rotate(45deg)"}}>
                <h1 className='font-poppins font-bold text-white text-sm mx-16'>Best Value</h1>
            </div>

            <div className='mt-7'>
                <h1 className='font-poppins text-bg-blue dark:text-white text-base'><DoneIcon color="success" className='mr-3' /> Unlike every other bo</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-base'><DoneIcon color="success" className='mr-3' /> Implemented double s</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-base'><DoneIcon color="success" className='mr-3' /> Securi priority when deve</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-base'><DoneIcon color="success" className='mr-3' /> Unlike every other bot w</h1>
                <h1 className='font-poppins text-bg-blue dark:text-white text-base'><span className='text-bg-red'><CloseIcon className='mr-3' /></span> Securi priority when deve</h1>
            </div>

            <div className='flex justify-center items-center font-poppins mt-3'>
                <h1 className='text-bg-blue dark:text-white font-bold text-3xl mr-1'>60<span className='text-lg'>$</span></h1>
                <h1 className='text-gray-300 font-normal text-base'> /MO</h1>
            </div>


            <div className={style.ocean}>
                <div className={style.wave} />
                <div className={style.wave} />
                <div className={style.wave} />
            </div>
            {/* <img width="320px" src={Purple} alt="" className='rounded-b-3xl'/> */}
        </div>
    )
}

export default ProPrice