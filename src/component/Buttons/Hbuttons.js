import React from 'react';
import { CButton } from './CButton';


function Hbuttons({step, setStep}) {
  return (
    <>
        {step === 0 ? <CButton setstep={setStep} index={0} style={{flex: '5 1 auto', opacity: '1'}}></CButton> : <CButton setstep={setStep} index={0}></CButton>}
        {step === 1 ? <CButton setstep={setStep} index={1} style={{flex: '5 1 auto', opacity: '1'}}></CButton> : <CButton setstep={setStep} index={1}></CButton>}
        {step === 2 ? <CButton setstep={setStep} index={2} style={{flex: '5 1 auto', opacity: '1'}}></CButton> : <CButton setstep={setStep} index={2}></CButton>}
    </>

  )
}
export default Hbuttons