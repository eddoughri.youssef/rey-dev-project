import React, { useState } from 'react'


import style from "./tech.module.css";

import activateBtn from '../../../media/activate.png'; 
import normalBtn from '../../../media/nrml_btn.png'; 


function Tech() {
    const dumpAction = ()=>{
        console.log("clicked")
    }
    const left_btns = [dumpAction,dumpAction,dumpAction,dumpAction]
    const right_btns = [dumpAction,dumpAction,dumpAction,dumpAction]
    const [clicked, setClicked] = useState({pos: null, index: null});
    return (
        <section className={style.tech}>
            <header>
                <h1>Alien Tech</h1>
                <p>We're announcing a new type of community designed to reward public goods and build a sustainable future for Ethereum</p>
            </header>
            <div className={style.screen}>
                <div className={style.btns}>
                    {left_btns.map((elem, index)=>{
                        const cnd = (clicked.index === index && clicked.pos == 'left');
                        return (
                            <button
                                onClick={(event)=>{
                                    setClicked({pos: "left", index})
                                    elem();
                                }}
                                className={cnd ? style.active : style.nrml}
                                key={elem+index+'left'}
                            >
                                <img src={cnd ? activateBtn : normalBtn} alt="video button" />
                            </button>
                        )
                    })}
                </div>
                <div className={style.moniter}>
                    <p>Play media from btn {clicked.index+1 + " " + clicked.pos}</p>
                </div>
                <div className={style.btns}>
                {right_btns.map((elem, index)=>{
                        const cnd = (clicked.index === index && clicked.pos == 'right');
                        return (
                            <button
                                onClick={(event)=>{
                                    setClicked({pos: "right", index})
                                    elem();
                                }}
                                className={cnd ? style.active : style.nrml}
                                key={elem+index+"right"}
                            >
                                <img src={cnd ? activateBtn : normalBtn} alt="video button" />
                            </button>
                        )
                    })}
                </div>
            </div>
        </section>
    )
}

export default Tech;