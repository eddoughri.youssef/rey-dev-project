import React, {useEffect, useState, useContext} from "react";

import { Box } from "@mui/system";
import { Button } from "@mui/material";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import Typography from "@mui/material/Typography";
import FilledInput from "@mui/material/FilledInput";


import { PaymentInfoContext } from "../../pages/payment";

import { getTheToken, getUserInfo, getAllOwnedServers } from "../../utils/discordAuthentification";
import { getCustomer, createCustomer } from '../../utils/handleCustomers';
import { getSubscription } from '../../utils/handleSubscriptions';
import { getPlans } from "../../utils/handlePlans";

import disocrd from "../../media/discord.png";
import { Place } from "@mui/icons-material";

function PaymentForm() {
  const [paymentInfo, pinfoContext] = useContext(PaymentInfoContext);
  const [userIn, setUserIn] = useState(false);
  const [listOfServers, setListOfServers] = useState([]);
  const [servers, setServers] = React.useState([]);
  const setTheData = async () => {
    try{
      const userInfo = await getUserInfo(); // returns the user info if the token is valid and false otherwise
      const servers = await getAllOwnedServers(); // returns the servers owend if the token is valid and false otherwise
      setListOfServers(servers.map(server => server.name));

      // heres update the context with the user info and the servers
      const payload = {
        discordId: userInfo.id, // the user id
        username: userInfo.username, // the user name
        email: userInfo.email, // the user email
        avatarUrl: userInfo.avatar !== null && `https://cdn.discordapp.com/avatars/${userInfo.id}/${userInfo.avatar}.png`, // the user avatar url
        servers: servers.map(server => server.name), // the servers owned by the user
      };
      try{
        // // console.log("triggred")
        // const customerData = {
        //   discordId: paymentInfo.Personal.id,
        //   customer: payload
        // }
        const customer = await getCustomer(payload.discordId);
        console.log("customer: ", customer)
        if(customer?.status === "NOT_FOUND"){
          // console.log("customer is null")
          // create customer
          console.log("user does not exist, creating user");
          const customer = await createCustomer(payload);
          console.log("customer: ", customer)
          if(customer === null){
            console.log("error creating user");
            return;
          }
        }else if(customer?.status === "MULTIPLE_FOUND"){
          console.log("multiple users found");
          return;
        }else if(customer?.status === "FOUND"){
          console.log("user exists");
          //  updata reducer subscription
          const subscription = await getSubscription(customer.data.Subscription);
          const payload = subscription.data;
          pinfoContext({type: "Subscribtion", payload});
          if(subscription.data.status){
            const response = await getPlans(subscription.data.plan);
            if(response.status === "FOUND"){
              const serversSelected = response.data.servers;
              pinfoContext({type: "ServersSelected", payload: serversSelected});
              const payloadPlan = {
                plan: response.data.name,
                botName: response.data.botname,
              }
              console.log("payloadPlan: ", payloadPlan)
              pinfoContext({type: "Plan", payload: payloadPlan});
            }
          }
        }
      }catch(err){
        console.log("error creating user");
        console.log(err);
        return;
      }
      pinfoContext({type: "Personal", payload});
      
      setUserIn(true);
    }catch(err){
      console.log("failed to fetch user info"); // meaning the user is not loged in
      console.log(err);
      setUserIn(false);
    }
  };
  const generateToken = async (grantCode) => {
    const flag = await getTheToken(grantCode); // returns true if the token is fetched successfully and false otherwise
    if(!flag){
      console.error("code is not valid");
    }
  };
  const handleChange = (event) => {
    // update the context with the selected servers
    const {
      target: { value },
    } = event;
    setServers(
      typeof value === "string" ? value.split(", ") : value
    );
    pinfoContext({type: "ServersSelected", payload: value});
  };
  
  const handleDiscordAuth = async ({target}) => {
    const client_id = process.env.REACT_APP_DISCORD_CLIENT_ID;
    const response_type = "code";
    const redirect_uri = process.env.REACT_APP_DISCORD_REDIRECT_URI;
    const scope = process.env.REACT_APP_DISCORD_SCOPE.split(" ").join("%20");
    const baseUrl = `https://discord.com/api/oauth2/authorize`;
    const URL = `${baseUrl}?client_id=${client_id}&redirect_uri=${redirect_uri}&response_type=${response_type}&scope=${scope}`; 
    window.location.replace(URL);
  }
  
  useEffect(() => {
    const urlParms = new URLSearchParams(window.location.search);
    const grantCode = urlParms.get("code");
    const error = urlParms.get("error");
    if(error){
      // if there is an error from the discord auth redirecting back to the app
      const description = urlParms.get("error_description");
      alert(error + " " + description)
      console.log("error", error);
      console.log("error_description", description);
      return;
    }
    else if(grantCode){
      generateToken(grantCode);
    }
    // if the user is already logged in meaning the access token is in the local storage or the global context has the user info
    // check whether the access token is valid
    // if it is valid then complete process
    // if it is not valid then clean up the local storage and start the process again
    if(paymentInfo.Personal){
      setListOfServers(paymentInfo.Personal.servers);
      setServers(paymentInfo.ServersSelected);
      setUserIn(true);
    }else{
      const access_token = localStorage.getItem("access_token");
      if(access_token){
        console.log("this is the way")
        setTheData();
      }
    }
  }, []);

  return (
    <Box
      sx={{
        flex: "10 1 auto",
        display: "flex",
        flexDirection: "column",
        justifyContent: "start",
        gap: "15%",
        marginTop: "5%",
      }}
    >
      <Box>
        <Typography
          sx={{
            fontFamily: "Poppins",
            // fontSize: "large",
            marginLeft: ".75rem",
            marginBottom: ".5rem"
          }}
        // className="font-poppins text-lg font-normal ml-10"
        >
          Log In
        </Typography>
        <Button
          variant="contained"
          onClick={handleDiscordAuth}
          sx={{
            fontWeight: '400',
            fontFamily: 'fira code, monospace',
            backgroundColor: '#5865F2',
            dispaly: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            gap: '15px',
            '&>span':{
              paddingRight: '2vw',
              fontSize: '.8rem',
            }
          }}
        >
          <img src={disocrd} alt="discord logo" />
          <span>{"Connect with Discord"}</span>
        </Button>
      </Box>
      <Box>
        <Typography 
          sx={{
            fontFamily: "Poppins",
            // fontSize: "large",
            marginLeft: ".75rem",
            marginBottom: ".5rem"
          }}>
            Select Server(s):
        </Typography>
        <Select
          value={servers}
          multiple
          onChange={handleChange}
          input={<FilledInput label="Name" />}
          disabled={!userIn}
          sx={{
            color: 'white',
            backgroundColor: '#2C2C2C',
            minWidth: '75%',
            // maxHeight: '55%',
            fontFamily: 'fira code, monospace',
            borderRadius: '12px',
            '&:focus-within, &:hover':{
              backgroundColor: '#4C4C4C',
            },
            '&:focus-within':{
              borderRadius: '12px 12px 0 0'
            },
            '&>ul':{
              backgroundColor: '#2C2C2C',
            }
          }}
          >
            {listOfServers.map((server, index)=>{
              return <MenuItem value={server} key={index+server}>{server}</MenuItem>
            })}
        </Select>
      </Box>
    </Box>
  );
}

export default PaymentForm;
