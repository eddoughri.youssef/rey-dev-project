import React from 'react'
import CommunPrice from './pricing/CommunPrice'
import ProPrice from './pricing/ProPrice'
import EntreprisePrice from './pricing/EntreprisePrice'

import style from './pricing.module.css';

function Pricing() {
    return (
        <div data-aos="fade-up" data-aos-duration="4000" className={style.pricing} id="pricing">
            <h1 className={'text-5xl md:text-header 2xl:text-5xl font-bold text-bg-blue dark:text-white text-center' + style}>Pricing</h1>
            <div className="flex flex-col lg:flex-row justify-center items-center mt-4">
                <CommunPrice />
                <ProPrice />
                <EntreprisePrice />
            </div>
        </div>
    )
}

export default Pricing;