import React from "react";

import Box from "@mui/material/Box";

function index({ title, text }) {
  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        gap: "1.3rem",
      }}
    >
      <h1 className="font-poppins text-5xl font-bold">{title}</h1>
      <p className="font-poppins text-lg font-normal">{text}</p>
    </Box>
  );
}

export default index;
