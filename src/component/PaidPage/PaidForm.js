import React, { useEffect, useContext, useState, useRef } from "react";

import { Box } from "@mui/system";
import Button from "@mui/material/Button";


import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';


import { PaymentInfoContext } from "../../pages/payment";

function MainPage({handleCancelSubscription, handleUpdateCard}){
    return (
        <>
            <Button 
                variant="contained"
                onClick={handleCancelSubscription}
                sx={{
                    backgroundColor: '#4c4c4c',
                    color: 'white',
                    padding: '16px 10px 16px',
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                    fontFamily: 'fira code, monospace',
                    borderRadius: '12px',
                }}>
                Cancel Subscription
            </Button>
            <Button 
                variant="contained"
                onClick={handleUpdateCard}
                sx={{
                    background: 'linear-gradient(90deg, #41ABF3, #3C19B9)',
                    color: 'white',
                    padding: '16px 10px 16px',
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                    fontFamily: 'fira code, monospace',
                    borderRadius: '12px',
                }}>
                Update Payment Card
            </Button>
        </>
    )
}

function CancelPage() {   
    const [pinfoContext, setPinfoContext] = useContext(PaymentInfoContext);

    const handleCancelSubscription = async () => {
        // cancel subscription
        // redirect to home page
        const response = await fetch(`http://localhost:5000/subscription/cancel?discordId=${pinfoContext.Personal.discordId}`, {
            method: 'GET',
        });
        const data = await response.json();
        console.log(data);
        if(data.error){
            alert("ERROR");
        }else{
            alert("SUCCESS");
            window.location.replace("http://localhost:3000");
        }
    }
    return (
            <Button 
            variant="contained"
            onClick={handleCancelSubscription}
            sx={{
                backgroundColor: 'red',
                color: 'white',
                padding: '16px 10px 16px',
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-evenly',
                fontFamily: 'fira code, monospace',
                borderRadius: '12px',
            }}>
            You Are About To Cancel Your Subscription
        </Button>
    )
}


function UpdatePage() {
  const stripe = useStripe();
  const elements = useElements();
  const cardElementRef = useRef(null);
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (event) => {
    event.preventDefault();

    setLoading(true);

    // Get the subscription ID from your application state or from the URL query parameter
    const subscriptionId = 'your_subscription_id_here';

    const { error, paymentMethod } = await stripe.createPaymentMethod({
      type: 'card',
      card: elements.getElement(CardElement),
    });

    if (error) {
      // Handle any errors that may occur during the process
      console.log(error);
      setLoading(false);
      return;
    }

    // Get the payment method ID and subscription ID from your application state or from the URL query parameter
    // const { success, message } = await updateSubscription(subscriptionId, paymentMethod.id);

    // if (success) {
    //   // Display a success message to the user
    //   alert(message);
    // } else {
    //   // Display an error message to the user
    //   alert(message);
    // }

    setLoading(false);
  };
  
  return (
    <form onSubmit={handleSubmit}>
      <div ref={cardElementRef}>
        <CardElement />
      </div>
      <button type="submit" disabled={loading}>
        {loading ? 'Updating Card...' : 'Update Card'}
      </button>
    </form>
  );
}
// function UpdatePage() {

//     const stripe = loadStripe("pk_test_51MkB17ExWnmSDDUn1lYGheTTEocrFOYIzcKSD898HDO1wMOmIK4T3gKZXfSRpHhgpm36jQyGXTiONoXtn7GygFIS00AdrBHGIF");
//     const elements = stripe.elements();
//     const cardElement = elements.create('card');


//     const handleSubmit = async (event) => {
//         event.preventDefault();

//         // Get the subscription ID from your application state or from the URL query parameter
//         const { error, paymentMethod } = await stripe.createPaymentMethod({
//         type: 'card',
//         card: cardElement,
//         });

//         if (error) {
//             // Handle any errors that may occur during the process
//             console.log(error);
//             return;
//         }

//         // Get the payment method ID and subscription ID from your application state or from the URL query parameter
//         const { success, message } = await updateSubscription(subscriptionId, paymentMethod.id);

//         if (success) {
//             // Display a success message to the user
//             alert(message);
//         } else {
//             // Display an error message to the user
//             alert(message);
//         }
//     }
//     return (
//         <>

//         </>
//     )


// }
function PaidPage() {
    const handleCancelSubscription = () => {
        console.log("cancel subscription");
        setPath({
            status: true,
            component: <CancelPage />
        });
    }
    const handleUpdateCard = () => {
        console.log("update card");
        setPath({
            status: true,
            component: <UpdatePage  />
        });
    }
    const Backward = () => {
        console.log("cancle canclation");
        setPath({
            status: false,
            component: <MainPage handleCancelSubscription={handleCancelSubscription} handleUpdateCard={handleUpdateCard}/>
        });
    }
    const [path, setPath] = useState({
        status: false,
        component: <MainPage handleCancelSubscription={handleCancelSubscription} handleUpdateCard={handleUpdateCard}/>
    });
    return (
        <Box
        sx={{
            flex: "10 1 auto",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            gap: "15%",
        }}
        >
            {path.component}
            {path.status ? <Button
                variant="contained"
                onClick={Backward}
                sx={{
                    backgroundColor: '#4c4c4c',
                    color: 'white',
                    padding: '16px 10px 16px',
                    width: '20%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                    fontFamily: 'fira code, monospace',
                    borderRadius: '12px',
                }}>
                    Back
            </Button> : ""}
        </Box>
    );
}

export default PaidPage;