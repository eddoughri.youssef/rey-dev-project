import PaymentHeader from "../PaymentHeader";

function PaidHeader() {
  const title = "First step, give us more info";
  const text =
    "To finalize your subscription, kindly complete your payment using a valid credit card.";
  return PaymentHeader({ title, text });
}

export default PaidHeader;
