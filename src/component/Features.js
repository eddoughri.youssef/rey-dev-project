import React, {useState} from 'react'

import {combineCssClasses} from "../shortcut"

import style from "./features.module.css";



import features_header_img from "../media/features.png";
import nh_logo_jisa from "../media/jisa_logo.png";
import playbtn from "../media/playButton.png";

function VideoPlayer({bg_image_code, updateThePlayerState}){
  const [selectedVideo, setSelectedVideo] = useState(1);
  const bg_image = `https://img.youtube.com/vi/${bg_image_code[selectedVideo]}/hqdefault.jpg`;
  return (
    <>
      <div className={style.videoPlayer_box}
          style={{
            backgroundImage: `url(${bg_image})`
          }}>
        <img src={playbtn} alt="Video Player button" onClick={(event)=>{
          updateThePlayerState(bg_image_code[selectedVideo]);
        }}/>
      </div>

      <div className='flex justify-center items-center gap-[5%]'>
        <button 
          className={'h-[1rem] min-w-[1rem] rounded-full transition-all delay-75' + (selectedVideo===0 ? ' w-12 bg-white' : ' bg-gray-500')}
          onClick={()=>setSelectedVideo(0)}
          />
        <button 
          className={'h-[1rem] min-w-[1rem] rounded-full transition-all delay-75' + (selectedVideo===1 ? ' w-12 bg-white' : ' bg-gray-500')}
          onClick={()=>setSelectedVideo(1)}
          />
        <button 
          className={'h-[1rem] min-w-[1rem] rounded-full transition-all delay-75' + (selectedVideo===2 ? ' w-12 bg-white' : ' bg-gray-500')}
          onClick={()=>setSelectedVideo(2)}
          />
      </div>

    </>
  )
}


function Livesupport({itemClass, img, updateThePlayerState, ytb_video_code, data}){
  return (
    <div data-aos="fade-up" data-aos-duration="4000" className={combineCssClasses(itemClass, style.support)}>
      <header>
        <img src={img} alt="image described this feature"/>
      </header>
      <div className={style.desc}>
        <div className={style.title}>
          {data.title}
        </div>

        <div className={style.describe}> 
          <ul>
            {data.points.map(
              (elem, index) => <li key={elem+index}>{elem}</li>
            )}
          </ul>
        </div>
      </div>
      <div className={style.video}>
          <VideoPlayer bg_image_code={ytb_video_code} updateThePlayerState={updateThePlayerState}/>
      </div>
    </div>
  );
}

function Media({itemClass, updateThePlayerState, ytb_video_code}){
  return (
    <div className={combineCssClasses(style.item, itemClass)}>
      <VideoPlayer bg_image_code={ytb_video_code} updateThePlayerState={updateThePlayerState}/>
    </div>
  );
}


function Card({itemClass, updateThePlayerState}){
  return (
    <div className={combineCssClasses(style.item, itemClass)}>
      <div className={style.message}>
        <h1>A BOT THAT <span className='line-through'>SUITS</span> EXCEEDS YOUR EXPECTATIONS</h1>
      </div>
      <div className={style.cardFooter}>
        <div className={style.linePiece} />
        <img src={nh_logo_jisa} alt="logo of jisa not hielighted" />
      </div>
    </div>
  );
}

function Features({updateThePlayerState}) {
  const data = [{
    title: "TicketEase",
    points: [
      "Introducing TicketEase – The Best Ticket Manager for your Discord community.",
      "Say goodbye to disorganized support requests and long wait times for your members. With TicketEase, you can provide a seamless support experience for your Discord community like never before. This powerful tool leverages the intelligence of GPT-3 to automate the support process, saving you time and resources.",
      "For the first time ever on Discord, TicketEase uses threads instead of channels, providing a more organized and efficient way to handle support tickets. And with custom ticketing and infinite scaling capabilities, TicketEase can grow and adapt to meet the demands of your community.",
      "TicketEase also includes a feedback and rating collection system, allowing you to continually improve the performance of your support system and provide an exceptional experience for your members.",
    ],
  },{
    title: "Customizable AI assistant.",
    points: [
      "With Your own Customizable AI assistant, Choose your own custom image and nickname, you can also  provide your members with a personalized, efficient and engaging support experience. This powerful tool leverages the intelligence of GPT-3 and Google Translation Services to provide real-time support and answer members' questions.",
      "The AI assistant will also answer new members about your company from a custom FAQ, engage in discussions, translate, summarize and offer fun commands, keeping your members entertained and informed.",
      "The AI assistant is highly customizable, allowing you to tailor its responses to the specific needs of your community. Whether you need to answer frequently asked questions, provide real-time updates, or offer a fun and engaging experience for your members, Your own Customizable AI assistant has got you covered.",
    ]
  },{
    title: "The Peace of Mind you need: Security",
    points: [
      "Keep your company assets and community secure with Jisa.io. Our cutting-edge solution is designed with security in mind, providing you with the peace of mind you need.",
      "We understand the importance of keeping your assets and community safe. That's why we've coded with security at the forefront and implemented multiple layers of security built on top of the critical commands."
    ]
  }]
  return (
    <section id="tech" className={style.features}>
      <header>
        <h1>ALIEN TECH</h1>
        <p className='font-poppins font-light text-lg text-center text-white'>
          We're announcing a new type of community designed to reward public goods and build a sustainable future for Ethereum
        </p>
      </header>
      <div className={style.cards}>
        <Livesupport data={data[0]} itemClass={style.livesupport} img={features_header_img} updateThePlayerState={updateThePlayerState} ytb_video_code={["9PvIIn6cc1M", "fJ5U3Q3PZ4s", "Dd0U5FQ0eLM"]} />
        <Livesupport data={data[1]} itemClass={style.livesupport2} img={features_header_img} updateThePlayerState={updateThePlayerState} ytb_video_code={["rkI034qKG7o", "voESIXYrJCI", "SufsPZJ6QqM"]}/>
        <Media itemClass={style.videoPlayer} updateThePlayerState={updateThePlayerState} ytb_video_code={["_fbq8RaKlxI", "vYsiBUsxhW0", "ixnH2MjRaAc"]}/>
        <Livesupport data={data[2]} itemClass={style.livesupport3} img={features_header_img} updateThePlayerState={updateThePlayerState} ytb_video_code={["o-llwv4pLrg", "fiXVcM1tHYY", "mPymRFeTJa4"]}/>
        <Card itemClass={style.quote} updateThePlayerState={updateThePlayerState}/>
      </div>
    </section>
  )
}

export default Features;