import React from "react";

import logo from "../media/jisa_logo.svg";

import style from "./header.module.css";

import logo1 from "../media/vee.svg";
import logo2 from "../media/optimism-red.svg";
import logo3 from "../media/eco-.svg";
import logo4 from "../media/gameloft-logo-vector.svg";
import { Link } from "react-router-dom";

function Header() {
  const logos = [logo1, logo2, logo3, logo4];
  const BAR = () => (
    <div className="bg-white w-10 md:w-[2px] h-[2px] md:h-5"></div>
  );

  return (
    <header id="home" className={style.layout}>
      <img src={logo} alt="jisa logo" />
      <div className={style.header_title}>
        <h1>
          <div className={style.words}>
            <span>Better</span>
            <span>Safer</span>
            <span>Cooler</span>
          </div>
          Discord
        </h1>
        <p className="font-poppins font-light text-lg text-center text-white max-w-[90%]">
          Utilizing bots to empower your Project through a Safe, User-Friendly
          and AI-Enabled Experience.
        </p>
        <div class="mt-12 flex items-center justify-center gap-x-6">
          <a
            href="#"
            className="rounded-full bg-transparent border-solid border-2 border-[#41ABF3] px-[10vw] md:px-[4rem] py-[0.5rem] font-bold text-white shadow-sm focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 font-poppins text-lg"
          >
            Demo
          </a>
          <Link
            to="/payment"
            className="rounded-full bg-gradient-to-r from-[#3C19B9] to-[#41ABF3] px-[10vw] md:px-[4rem] py-[0.5rem] font-bold text-white shadow-sm hover:bg-gradient-to-r hover:from-[#3C19B9] hover:to-[#3057a0] focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 font-poppins text-lg"
          >
            Buy
          </Link>
        </div>
      </div>
      <div className="mt-20 md:mt-auto flex flex-col md:flex-row justify-evenly items-center gap-[20px] w-[90vw]">
        {logos.map((elem, index) => {
          return (
            <>
              <img
                className="max-w-[90px] sm:max-w-[15vw] md:max-w-[100px] opacity-40"
                key={index + elem}
                src={elem}
              />
              {index !== logos.length - 1 && <BAR />}
            </>
          );
        })}
      </div>
    </header>
  );
}

export default Header;
