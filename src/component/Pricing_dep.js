import React from 'react'

import style from "./pricing.module.css";

import {combineCssClasses} from '../../../shortcut/';

function Card({title, className, pros, cons, tag}){
    return (
        <div className={combineCssClasses(style.card, className) }>
            <div className={style.header}>
                {title}
            </div>
            <div className={style.prosAndCons}>
                <ul>
                    {pros.map((element, index) => {
                        return (
                            <li className={style.pros}>{element}</li>
                        )
                    })}
                </ul>
                <ul>
                    {cons.map((element, index) => {
                        return (
                            <li className={style.cons}>{element}</li>
                        )
                    })}
                </ul>
            </div>
            <div className={style.tag}>{tag}</div>
            <div className={style.ocean}>
                <div className={style.wave}/>
                <div className={style.wave}/>
                <div className={style.wave}/>
            </div>
        </div>
    )
}


function Pricing() {
    const dumpFeatures = ["Unlike every other bo", "Implemented double s", "Securi priority when deve", "Unlike every other bot w", "Securi priority when deve"];
    const Tag = ({price})=>{
        return (
            <div>
                <h1>{price}</h1><span>$</span>
            </div>
        )
    }
    const cards = {
        commun:{
            title: "Commun",
            pros: dumpFeatures.slice(0,3),
            cons: dumpFeatures.slice(3),
            tag: <Tag price="30" />
        },
        pro:{
            title: "Pro",
            pros: dumpFeatures.slice(0, 4),
            cons: dumpFeatures.slice(4),
            bestDeal: true,
            tag: <Tag price="60" />
        },
        enterprise:{
            title: "Enterprise",
            pros: dumpFeatures,
            cons: [],
            tag: <Button />
        }
    }
    return (
        <section className={style.pricing}>
            <header>
            <h1>Pricing</h1>
            </header>
            <div className={style.cards}>
                <Card {...cards.commun} className={style.secondary}/>
                <Card {...cards.pro} className={style.primary}/>
                <Card {...cards.enterprise} className={style.secondary}/>
            </div>
        </section>
    )
}
export default Pricing;