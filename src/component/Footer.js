import React from 'react';

import style from "./footer.module.css";
import { Link } from 'react-router-dom';

import logo from "../media/Logo_jisa.png"
import twitter from "./media/twitter.png";
import discord from "./media/discord.png";


import { combineCssClasses } from '../shortcut';

function footer(props) {
    // console.log(props);
    return (
        <>
            <div className={combineCssClasses(style.ellipse_box, style.position_bottom_left, style.bg_blue)} />
            <div className={combineCssClasses(style.ellipse_box, style.position_bottom_center, style.bg_purble)} />
            <div className={combineCssClasses(style.ellipse_box, style.position_bottom_right, style.bg_green)} />
            <footer
                className={ style.basicContainer +
                            " main-page-footer " +
                            style.mainPageStyle
                }>
                <div className={style.holder}>
                    <div className={style.logo}>
                        <div className={style.name} >
                            <img src={logo} alt="Jisa logo" />
                        </div>
                        <div className={style.copyright}>
                            <p>Copyright © 2022 X LLC.</p>
                            <p>All Rights Reserved.</p>
                        </div>
                    </div>
                    <div className={style.main}>
                        <ul className={style.nav}>
                            <li><a href="#home">Home</a></li>
                            <li><a href="#tech">Tech</a></li>
                            <li><a href="#features">Features</a></li>
                            <li><a href="#pricing">Pricing</a></li>
                        </ul>
                        <div className={style.companyInfo}>
                            <div className={style.complianceInfo}>
                                <p>Privacy Policy</p>
                                <p>Terms & Conditions</p>
                            </div>
                            <div className={style.aboutSite}>
                                <div className={style.mindBlowingDiv}>
                                    <p>Designed & Developed</p>
                                    <p>by Jisa</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={style.links}>
                        <div className={style.title}>
                            <p className='font-poppins font-light text-white'>Follow US</p>
                        </div>
                        <div className={style.Alinks}>
                            <ul>
                                <li><Link to="/"><img src={discord} alt={'Icons of discord'} /></Link></li>
                                <li><img src={twitter} alt={'Icons of twitter'} /></li>
                                {/* <li><img src={linkedin} alt={'Icons of linkedin'} /></li> */}
                                {/* <li><img src={vector} alt={'Icons of vector'} /></li> */}
                            </ul>
                            <ul>
                                {/* <li><img src={facebook} alt={'Icons of facebook'} /></li> */}
                                {/* <li><img src={github} alt={'Icons of github'} /></li> */}
                                {/* <li><img src={youtube} alt={'Icons of youtube'} /></li> */}
                                {/* <li><img src={whatsapp} alt={'Icons of whatsapp'} /></li> */}
                            </ul>
                        </div>
                    </div>
                </div> 
            </footer>
        </>
    )
}

export default footer