import React, { useEffect } from 'react';

import { Outlet } from 'react-router-dom';
import style from "./skeleton.module.css";

// helpfull css classes
import { combineCssClasses } from "./shortcut/index.js";


// shapes
// import shape1_top from "./media/shape1_top.png";
// import shape2_med_right from "./media/shape2_med_right.png";
// import shape2_med_left from "./media/shape2_med_left.png";
// import shape3_med_right from "./media/shape3_med_right.png";

import AOS from 'aos';

function Skeleton() {

  useEffect(()=>{
    AOS.init();
    AOS.refresh();
    document.addEventListener('aos:in', ({ detail }) => {
      console.log('animated in', detail);
    });

    // for shapes animation

    // document.addEventListener('scroll', (e)=>{
    //   const FULL_HEIGHT = document.body.scrollHeight;
    //   const WINDOW_HEIGHT = isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight;
    //   const HEIGHT = FULL_HEIGHT - WINDOW_HEIGHT + 30 ;
    //   const CurrHeight = window.scrollY;
    //   console.log("scrolled")
    //   console.log(HEIGHT, CurrHeight)
    // })
  },[]);
  return (
    <>

      {/* render the children */}
      <Outlet />
      {/* done */}

      {/* shapes and colors */}
      {/* <div className={combineCssClasses(style.ellipse_box, style.position_top_left, style.bg_blue)} /> */}
      <div className={combineCssClasses(style.ellipse_box, style.position_top_center, style.bg_purble)} />
      <div className={combineCssClasses(style.ellipse_box, style.position_top_right, style.bg_green)} />
       
      {/* <img src={shape1_top} alt="random shape" className={combineCssClasses(style.shapes_top, style.top_left)}/>
      <img src={shape2_med_right} alt="random shape" className={combineCssClasses(style.shapes_top, style.med_right)}/>
      <img src={shape2_med_left} alt="random shape" className={combineCssClasses(style.shapes_top, style.med_left)}/>
      <img src={shape3_med_right} alt="random shape" className={combineCssClasses(style.shapes_top, style.med2_right)}/> */}


      {/* <div className={combineCssClasses(style.ellipse_box, style.position_bottom_left, style.bg_blue)} />
      <div className={combineCssClasses(style.ellipse_box, style.position_bottom_center, style.bg_purble)} />
      <div className={combineCssClasses(style.ellipse_box, style.position_bottom_right, style.bg_green)} /> */}
      {/* done */}
    </>
  )
}

export default Skeleton;