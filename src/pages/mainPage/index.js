import React from 'react'


import Footer from '../../component/Footer';
import Header from '../../component/Header';
import Features from '../../component/Features';
import Tech from '../../component/Tech';
import Compliance from '../../component/Compliance';
import Pricing from '../../component/Pricing'


function Index({updateTheState, showPlayer}) {
  return (
    <div id='mainPage' className={showPlayer.play ? 'opacity-25': 'opacity-100'}>
        <Header />
        <Features updateThePlayerState={updateTheState}/>
        <Tech />
        <Compliance />
        <Pricing />
        <Footer/>
    </div>
  )
}

export default Index;