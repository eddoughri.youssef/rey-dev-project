import React, { useState, useEffect, useContext } from 'react'

import { Box } from '@mui/system';
import { Button } from '@mui/material';
import Hbuttons from '../../component/Buttons/Hbuttons';

import PurchaseFormHeader from '../../component/purchase/PurchaseHeaderForm';
// import PurchaseForm from '../../component/purchase/PurchaseForm';
import PurchaseFormStrip from '../../component/purchase/PurchaseFormStrip';

// import { PaymentElement } from '@stripe/react-stripe-js';
// import { LinkAuthenticationElement } from '@stripe/react-stripe-js';
import { useStripe, useElements } from '@stripe/react-stripe-js';

// import { PaymentInfoContext } from "./payment/index";

function Payment({step, setStep, paymentInfo}) {
    const stripe = useStripe();
    const elements = useElements();

    const [email, setEmail] = useState('');
    const [message, setMessage] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    // const [paymentInfo, pinfoContext] = useContext(PaymentInfoContext);

    useEffect(() => {
        if (!stripe) {
            return;
        }
        // clientSecret should be retrived from local storage since the parent component
        // should have already created the payment intent
        const clientSecret = localStorage.getItem("clientSecret");
        if (!clientSecret) {
            return;
        }
        stripe.retrievePaymentIntent(clientSecret).then(({ paymentIntent }) => {
        // switch (paymentIntent.status) {
        //     case "succeeded":
        //         console.log(paymentIntent)
        //         alert("Payment succeeded!");
        //         break;
        //     case "processing":
        //         alert("Your payment is processing.");
        //         break;
        //     case "requires_payment_method":
        //         alert("Your payment was not successful, please try again.");
        //         break;
        //     default:
        //         alert("Something went wrong.");
        //         break;
        // }
        });
    }, [stripe]);

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (!stripe || !elements) {
        // Stripe.js has not yet loaded.
        // Make sure to disable form submission until Stripe.js has loaded.
        return;
        }

        setIsLoading(true);

        const confirmPayment = await stripe.confirmPayment({
            elements,
            confirmParams: {
                // Make sure to change this to your payment completion page
                return_url: "http://localhost:3000",
            },
        });

        const error = confirmPayment.error;

        // This point will only be reached if there is an immediate error when
        // confirming the payment. Otherwise, your customer will be redirected to
        // your `return_url`. For some payment methods like iDEAL, your customer will
        // be redirected to an intermediate site first to authorize the payment, then
        // redirected to the `return_url`.
        if (error.type === "card_error" || error.type === "validation_error") {
            alert(error.message);
        } else {
            alert("An unexpected error occurred.");
        }

        setIsLoading(false);
    };

    const paymentElementOptions = {
        layout: "tabs"
    }
    return (
        <>
            <Box
            sx={{
                display: 'grid',
                gridTemplateRows: '2fr 4fr 1fr',
                flexDirection: 'column',
                gap: '3vh',
            }}
            >
                <PurchaseFormHeader />
                <Box sx={{
                    display: 'flex',
                    justifyContent: "start",
                    alignItems: "stretch",
                    gap: "20px",
                }}>
                    <Box
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "7px",
                        minHeight: "100%",
                        '&>button':{
                        width: '2px',
                        flex: '1 1 auto',
                        backgroundColor: 'white',
                        borderRadius: '5px',
                        opacity: '0.5',
                        }
                    }}
                    >
                    <Hbuttons setStep={setStep} step={step}/>
                </Box>
                {/* <ThemeProvider theme={inputsFontChanger('fira code')}> */}
                <PurchaseFormStrip />
                {/* <LinkAuthenticationElement
                    id="link-authentication-element"
                    onChange={(e) => setEmail(e.target.value)}
                />
                <PaymentElement id="payment-element" options={paymentElementOptions} /> */}
                {/* </ThemeProvider> */}
            </Box>
            <Box
                sx={{
                marginLeft: 'auto',
                alignSelf: 'end',
                }}
                >
                <Button
                sx={{
                    marginLeft: 'auto',
                    fontFamily: 'Poppins',
                    fontWeight: 'bold',
                    color: 'white',
                    background: 'linear-gradient(90deg, #41ABF3, #3C19B9)',
                    borderRadius: '30px',
                    textTransform: 'none',
                    padding: '10px 60px'
                }}
                onClick={
                    (event)=>{
                        console.log("paymentInfo", paymentInfo);
                        handleSubmit(event);
                    }
                }
                >{"Pay"}</Button>
                </Box>
            </Box>
        </>
  )

}

export default Payment;