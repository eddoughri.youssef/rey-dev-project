import React, { createContext } from 'react'

import { Container, Box } from '@mui/system';


import PaymentForm from '../../component/authentificate/PaymentForm';
import PaymentFormHeader from '../../component/authentificate/PaymentHeaderForm';

import PlanForm from '../../component/plan/PlanForm';
import PlanFormHeader from '../../component/plan/PlanHeaderForm';

import logo from "../../media/jisa_logo.svg";
import DescribtionCard from '../../component/PaymentSideCard/DescribtionCard';
import SummerizeCard from '../../component/PaymentSideCard/SummerizeCard';

import FillingInfo from './FillingInfo';
import PaymentPage from './PaymentPage';




import { createPlan, updatePlan } from '../../utils/handlePlans';
import { updateSubscription } from '../../utils/handleSubscriptions';
import { getCustomer } from '../../utils/handleCustomers';


export const PaymentInfoContext = createContext();

function Payment() {
  function reducer(state, action){
    console.log("reducer trigger");
    switch(action.type){
      case "Personal":
        return {...state, Personal: action.payload};
      case "ServersSelected":
        return {...state, ServersSelected: action.payload};
      case "Plan":
        return {...state, Plan: action.payload};
      case "UpdatePlan":
        return {...state, Plan: {...state.Plan, plan: action.payload}};
      case "UpdateBotName":
        return {...state, Plan: {...state.Plan, botName: action.payload}};
      case "Payment":
        return {...state, Payment: action.payload};
      case "Subscribtion":
        return {...state, Subscribtion: action.payload};
      default:
        return state;
    }
  }
  const initalState = {
    Personal: null,
    ServersSelected: [],
    Plan: { plan: "Pro", botName: "" },
    Payment: null,
    Subscribtion: {
      status: false,
      plan: null,
    }
  }
  const [paymentInfo, disptacher] = React.useReducer(reducer, initalState);
  const [step, setStep] = React.useState(0);
  const pages = {
    header: [<PaymentFormHeader />, <PlanFormHeader />],
    form: [<PaymentForm />, <PlanForm />],
  }
  const nextPageController = async (nextPage)=>{
    if(paymentInfo.Subscribtion.status){
      // can go whenever he wants
      setStep(nextPage);
      return;
    }
    if(nextPage >= 2){
      if(paymentInfo.Personal === null){
        alert("Please fill all the information: please login first ");
      }else if(paymentInfo.ServersSelected.length === 0){
        alert("Please fill all the information: select at least one server to proced");
      }else if(paymentInfo.Plan.plan.toLowerCase() === "pro" && paymentInfo.Plan.botName === ""){
        alert("Please fill all the information: bot name");
      }else{
        // what if the user is not logged in ? 


        // console.log(paymentInfo)
        // paymentInfo.Subscribtion.status if true then the user is already subscribed
        // offer him to chose a new plan
        // the new component should handle this case


        // paymentInfo.Subscribtion.plan if false then the user is not subscribed
        // in this case create a new plan based on the paymentInfo use the endpoint /plan/create to create a new plan
        let plan = null;
        const payload={
          // name, description, servers, botname
          name: paymentInfo.Plan.plan,
          description: null,
          servers: paymentInfo.ServersSelected,
          botname: paymentInfo.Plan.botName,
        }
        if(paymentInfo.Subscribtion.plan === null){
          // create a new plan
          // use the endpoint /plan/create
          // after that proceed to the payment page
          console.log("customer dosn't have any active plan...create a new plan");
          plan = await createPlan(payload);
          if(plan === null){
            alert("something went wrong: failed to create a new plan");
            return;
          }
        }else{
          // update the old plan
          // use the endpoint /plan/update
          // after that proceed to the payment page
          console.log("customer has an old plan...update the plan");
          payload.currentPlanId = paymentInfo.Subscribtion.plan;
          plan = await updatePlan(payload);
          if(plan === null){
            alert("something went wrong: failed to update the plan");
            return;
          }
        }
        console.log(plan, payload)
        if(plan.status === "CREATED" || plan.status === "WARNING"){
          // after creating the plan or updating it update the subscribtion related to the user
          // in the database customer.subscribtion.plan = plan._id
          // getSubscription(plan._id, paymentInfo.Personal._id)
          const customer = await getCustomer(paymentInfo.Personal.discordId);
          // console.log("customer: ", customer)
          if(customer?.status === "NOT_FOUND" || customer?.status === "MULTIPLE_FOUND"){
            // console.log("customer is null")
            alert("something went wrong: failed to get the customer");
            return;
          }else if(customer?.status === "FOUND"){
            // console.log("user exists");
            //  updata reducer subscription
            const resp = await updateSubscription(plan.data, customer.data.Subscription);
            if(resp.status !== "UPDATED"){
              alert("something went wrong: failed to update the subscribtion");
              return;
            }
            disptacher({type: "Subscribtion", payload: resp.data});
          }
        }

        //   setStep(2);
        //   return;
        // }
        // and then use the endpoint /plan/delete to delete the old plan if the old plan is not nullw
        // after that proceed to the payment page
        setStep(2);
      }
    }else{
      setStep(nextPage);
    }
  }
  return (
    <PaymentInfoContext.Provider value={[paymentInfo, disptacher]}>
      <Container sx={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          // alignItems: 'center',
          minHeight: '100vh',
          gap: '5vh',
          maxWidth: {
            lg: "1500px"
          },
          '&>img':{
              maxWidth: 'max(15%, 140px)',
          },
      }}
      id="mainPage"
      >
        <img src={logo} alt="jisa logo" style={{marginTop: "7vh"}}/>
        <Box sx={{
          display: 'grid',
          gridTemplateColumns: '1fr 1fr',
          flexDirection: 'row',
          noWrap: true,
          alignItems: 'stretch',
          justifyContent: 'center',
          minHeight: '50vh',
          gap: '5vw',
        }}>
          {step !== 2 ? <FillingInfo pages={pages} setStep={nextPageController} step={step}/> : <PaymentPage setStep={nextPageController} step={step} paymentInfo={paymentInfo}/>}
          <Box
            sx={{
              background: 'linear-gradient(180deg, #FFFFFF66, #FFFFFF00)',
              boxShadow: '0px 4px 50px 0px #41ABF320',
              borderRadius: '5px',
              minHeight: '100%',
              minWidth: '100%',
              display: 'flex',
              flexDirection: 'column',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >    
            {step !== 2 ? <DescribtionCard step={step} /> : <SummerizeCard />}
          </Box>
        </Box>
      </Container>
    </PaymentInfoContext.Provider>
  )
}

export default Payment;


// chatgpt example
// import React, { useState } from 'react';
// import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';

// const SubscriptionForm = () => {
//   const [name, setName] = useState('');
//   const [email, setEmail] = useState('');
//   const [cardComplete, setCardComplete] = useState(false);
//   const stripe = useStripe();
//   const elements = useElements();

//   const handleSubmit = async (event) => {
//     event.preventDefault();

//     if (!stripe || !elements) {
//       return;
//     }

//     const cardElement = elements.getElement(CardElement);

//     const { error, paymentMethod } = await stripe.createPaymentMethod({
//       type: 'card',
//       card: cardElement,
//       billing_details: {
//         name: name,
//         email: email,
//       },
//     });

//     if (error) {
//       console.log(error);
//     } else {
//       console.log(paymentMethod);
//       // Call your backend API to create a subscription with the payment method ID
//     }
//   };

//   return (
//     <form onSubmit={handleSubmit}>
//       <label>
//         Name
//         <input
//           type="text"
//           value={name}
//           onChange={(e) => setName(e.target.value)}
//         />
//       </label>
//       <label>
//         Email
//         <input
//           type="email"
//           value={email}
//           onChange={(e) => setEmail(e.target.value)}
//         />
//       </label>
//       <label>
//         Card details
//         <CardElement
//           options={{
//             style: {
//               base: {
//                 fontSize: '16px',
//               },
//             },
//           }}
//           onChange={(e) => {
//             setCardComplete(e.complete);
//           }}
//         />
//       </label>
//       <button type="submit" disabled={!stripe || !cardComplete}>
//         Subscribe
//       </button>
//     </form>
//   );
// };

// export default SubscriptionForm;