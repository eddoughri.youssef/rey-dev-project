import React, { createContext } from 'react'

import { Box } from '@mui/system';
import { Button } from '@mui/material';
import Hbuttons from '../../component/Buttons/Hbuttons';


function FillingInfo({pages, step, setStep}) {
  return (
    <>
        <Box
        sx={{
            display: 'grid',
            gridTemplateRows: '2fr 4fr 1fr',
            flexDirection: 'column',
            gap: '3vh',
        }}
        >
            {pages.header[step]}
            <Box sx={{
                display: 'flex',
                justifyContent: "start",
                alignItems: "stretch",
                gap: "20px",
            }}>
                <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    gap: "7px",
                    minHeight: "100%",
                    '&>button':{
                    width: '2px',
                    flex: '1 1 auto',
                    backgroundColor: 'white',
                    borderRadius: '5px',
                    opacity: '0.5',
                    }
                }}
                >
                <Hbuttons setStep={setStep} step={step}/>

            </Box>
            {pages.form[step]}
        </Box>
        <Box
            sx={{
            marginLeft: 'auto',
            alignSelf: 'end',
            }}
            >
            <Button
            sx={{
                marginLeft: 'auto',
                fontFamily: 'Poppins',
                fontWeight: 'bold',
                color: 'white',
                background: 'linear-gradient(90deg, #41ABF3, #3C19B9)',
                borderRadius: '30px',
                textTransform: 'none',
                padding: '10px 60px'
            }}
            onClick={
                ()=>{
                    // if(step === 1){
                    //     // setStep(0); // popup screen to confirm payment
                    //     return;
                    // }
                    // else if(step === 1){
                    //     if(paymentInfo.Personal === null || paymentInfo.ServersSelected.length === 0){
                    //     console.error("error please authentificat first before going to the next step or chose at least one server");
                    //     return;
                    //     }
                    // }
                    setStep(step+1);
                }
            }
            >{"Next"}</Button>
            </Box>
        </Box>
    </>
  )
}

export default FillingInfo;