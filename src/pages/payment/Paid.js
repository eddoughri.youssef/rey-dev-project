import React, { useState, useEffect, useContext } from 'react'

import { Box } from '@mui/system';
import { Button } from '@mui/material';
import Hbuttons from '../../component/Buttons/Hbuttons';
import PaidHeader from '../../component/PaidPage/PaidHeader';
import PaidPage from '../../component/PaidPage/PaidForm';

// import PurchaseFormHeader from '../../component/purchase/PurchaseHeaderForm';
// import PurchaseForm from '../../component/purchase/PurchaseForm';
// import PurchaseFormStrip from '../../component/purchase/PurchaseFormStrip';

// import { PaymentElement } from '@stripe/react-stripe-js';
// import { LinkAuthenticationElement } from '@stripe/react-stripe-js';
// import { useStripe, useElements } from '@stripe/react-stripe-js';

// import { PaymentInfoContext } from "./payment/index";

function Paid({step, setStep, paymentInfo}) {
    return (
        <>
            <Box
            sx={{
                display: 'grid',
                gridTemplateRows: '2fr 4fr 1fr',
                flexDirection: 'column',
                gap: '3vh',
            }}
            >
                <PaidHeader />
                <Box sx={{
                    display: 'flex',
                    justifyContent: "start",
                    alignItems: "stretch",
                    gap: "20px",
                }}>
                    <Box
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                        gap: "7px",
                        minHeight: "100%",
                        '&>button':{
                        width: '2px',
                        flex: '1 1 auto',
                        backgroundColor: 'white',
                        borderRadius: '5px',
                        opacity: '0.5',
                        }
                    }}
                    >
                    <Hbuttons setStep={setStep} step={step}/>
                </Box>
                {/* <ThemeProvider theme={inputsFontChanger('fira code')}> */}
                <PaidPage/>
                {/* <LinkAuthenticationElement
                    id="link-authentication-element"
                    onChange={(e) => setEmail(e.target.value)}
                />
                <PaymentElement id="payment-element" options={paymentElementOptions} /> */}
                {/* </ThemeProvider> */}
            </Box>
            <Box
                sx={{
                marginLeft: 'auto',
                alignSelf: 'end',
                }}
                >
                {/* <Button
                sx={{
                    marginLeft: 'auto',
                    fontFamily: 'Poppins',
                    fontWeight: 'bold',
                    color: 'white',
                    background: 'linear-gradient(90deg, #41ABF3, #3C19B9)',
                    borderRadius: '30px',
                    textTransform: 'none',
                    padding: '10px 60px'
                }}
                onClick={
                    (event)=>{
                        console.log("paymentInfo", paymentInfo);
                        handleSubmit(event);
                    }
                }
                >{"Pay"}</Button>*/}
                </Box> 
            </Box>
        </>
  )

}

export default Paid;