import React, { useState, useEffect } from 'react'

import Payment from './Payment';


import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import { createSubscription } from '../../utils/handleSubscriptions';

import Paid from './Paid';

function PaymentPage(props) {
    // store the key in the .env file
    const stripePromise = loadStripe("pk_test_51MkB17ExWnmSDDUn1lYGheTTEocrFOYIzcKSD898HDO1wMOmIK4T3gKZXfSRpHhgpm36jQyGXTiONoXtn7GygFIS00AdrBHGIF");
    const [clientSecret, setClientSecret] = useState(null);
    useEffect(() => {
        // client secret should be retrived from the server
        // const clientSecret = localStorage.getItem("clientSecret");
        if(!props.paymentInfo.Subscribtion.status){
            console.log("props", props);
            const plan = props.paymentInfo.Plan.plan;
            const discordId = props.paymentInfo.Personal.discordId;
            createSubscription(discordId, plan).then((res) => {
                console.log("res", res);
                setClientSecret(res.data.clientSecret);
            }).catch((err) => {
                console.log("err", err);
            })
        }

    }, []);
    const appearance = {
        theme: 'night',
        variables: {
            colorBackground: '#2c2c2c',
            fontFamily: 'Poppins, fira code, Montserrat, monospace',
        },
        rules:{
            ".Input":{
                padding: "15px 20px 15px",
                // width: "99%",
                // fontFamily: 'fira code, monospace',
                borderRadius: '12px',
            },
            ".Label":{
                // padding: "15px 20px 15px",
                // width: "99%",
                // fontFamily: 'Poppins',
                fontSize: '1rem',
                marginLeft: ".75rem",
                marginBottom: ".3rem",
                marginTop: ".5rem",
            }
        }
    };
    const options = {
        clientSecret,
        appearance
    }
    // console.log("clientSecret", clientSecret);
    const Form = <Elements stripe={stripePromise} options={options}><Payment {...props} /></Elements>;
    const AlreadyPaid = <Elements stripe={stripePromise} options={options}><Paid {...props}/></Elements>;
    const Loadingx = <div>loading...</div>;
    // return AlreadyPaid;
    if(props.paymentInfo.Subscribtion.status) return (!clientSecret ? Loadingx : AlreadyPaid);
    return (!clientSecret ? Loadingx : Form)
}

export default PaymentPage;