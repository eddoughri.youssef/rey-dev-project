import React from 'react';

import { Box } from "@mui/system";
import Typography from '@mui/material/Typography';
import Collapse from '@mui/material/Collapse';


import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import StarBorder from '@mui/icons-material/StarBorder';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';

import Star from '../../media/Star.svg';
import Wallet from '../../media/Wallet.svg';
import Graph from '../../media/Graph.svg';
import Category from '../../media/Category.svg';
import LoginIcon from '@mui/icons-material/Login';
import logo from "../../media/jisa_logo.svg";


function Section({logo, selected, setSection}){
    const logos = {
        "Features": Star,
        "Analytics": Wallet,
        "Subscriptions": Graph,
        "Servers": Category,
    };
    const [open, setOpen] = React.useState(false);
    const handleClick = () => {
        setOpen(!open);
    };
    const subClicked = () => {
        setSection("Analytics");
    }
    if(logo === "Analytics"){
        return (
            <Box
            >
                <Box 
                    sx={{
                        padding: '10px',
                        borderRadius: '10px',
                        backgroundColor: selected ? '#D9D9D94C' : 'transparent',
                        cursor: 'pointer',
                        display: 'flex',
                        flexDirection: 'row',
                        gap: '20px',
                    }}
                    onClick={handleClick}>
                    
                    <img src={logos[logo]}/>
                    <Typography variant="h6"
                        sx={{
                            color: '#FFFFFF',
                            fontSize: '20px',
                            fontFamily: 'Poppins',
                            fontWeight: '500px',
                            display: 'flex',
                            justifyContent: 'space-between',
                            width: '100%',
                            alignItems: 'center',
                        }}>
                            {logo}
                            {open ? <ExpandLess /> : <ExpandMore />}
                    </Typography>
                </Box>
                
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <List component="div" disablePadding>
                        <ListItemButton sx={{ pl: 4 }} onClick={subClicked}>
                            <ListItemText primary="Sub 1" />
                        </ListItemButton>
                        <ListItemButton sx={{ pl: 4 }} onClick={subClicked}>
                            <ListItemText primary="Sub 2" />
                        </ListItemButton>
                        <ListItemButton sx={{ pl: 4 }} onClick={subClicked}>
                            <ListItemText primary="Sub 3" />
                        </ListItemButton>
                    </List>
                </Collapse>
            </Box>
        )
    }
    return (
        <Box
            sx={{
                padding: '10px',
                borderRadius: '10px',
                backgroundColor: selected ? '#D9D9D94C' : 'transparent',
                cursor: 'pointer',
                display: 'flex',
                gap: '20px',

            }}
            onClick={() =>{
                setSection(logo);
            }}>
            <img src={logos[logo]}/>
            <Typography variant="h6" sx={{
                color: '#FFFFFF',
                fontSize: '20px',
                fontFamily: 'Poppins',
                fontWeight: '500px',
            }}>
                    {logo}

            </Typography>
        </Box>
    )
}


export default function sideBar({ sections, section, setSection}) {
    return (
        <Box
            sx={{
                position: 'sticky',
                height: '95vh',
                top: '3px',
                // display: 'flex',
                // flexDirection: 'column',
                // paddingTop: '20px',
                // paddingLeft: '20px',
                // gap: '20%',
                display: 'grid',
                gridTemplateColumns: '1fr',
                gridTemplateRows: '3fr 5fr 3fr 2fr',
                paddingLeft: '10%',
            }}
        >
            <img src={logo} alt="jisa logo" style={{
                // height: '40px',
                width: '200px',
                alignSelf: 'center',
            }}/>
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '10px',
                    alignSelf: 'center',
                }}
            >
                {
                    sections.map(
                        (logo, index) => {
                            return (
                                <Section
                                    key={index}
                                    logo={logo}
                                    selected={section === logo}
                                    setSection={setSection}
                                />
                            );
                        }
                    )
                }
            </Box>
            
            <Box
                sx={{
                    padding: '10px',
                    borderRadius: '10px',
                    marginTop: 'auto',
                    backgroundColor: 'transparent',
                    cursor: 'pointer',
                    display: 'flex',
                    alignItems: 'center',
                    gap: '20px',
                    alignSelf: 'center',

                }}
            >
                <LoginIcon /> <Typography variant="h6" sx={{
                    color: '#FFFFFF',
                    fontSize: '15px',
                    fontFamily: 'Poppins',
                    fontWeight: '500px',
                    }}>
                        Log out
                </Typography>
            </Box>
    </Box>
  )
}