import React from 'react'

import { Box } from "@mui/system";
import { MinimizeTwoTone } from '@mui/icons-material';

export default function chartWrapper({children}) {
  return (
    <Box
        sx={{
            backgroundColor: '#121212',
            padding: '20px',
            borderRadius: '10px',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',            
            mt: '10px',
            '& canvas': {
                color: "#FFFFFF",
            },
            minWidth: '47%',

        }}
    >
        {children}
    </Box>
  )
}
