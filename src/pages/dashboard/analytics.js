import React, { useEffect, useState } from 'react';

import { Typography } from "@mui/material";
import Card from '@mui/material/Card';
import FormControl from '@mui/material/FormControl';
import NativeSelect from '@mui/material/NativeSelect';
import CardContent from '@mui/material/CardContent';
import Switch from '@mui/material/Switch';
import { Box } from "@mui/system";


import Chart from 'chart.js/auto';
import { 
    Line,
    Pie,
    Bar,
    Bubble,
    Doughnut,
    Radar,
    PolarArea,
    Scatter,
} from 'react-chartjs-2';


import Wrapper from './chartWrapper';

const data1 = {
    datasets: [
        {
            label: 'My First Dataset',
            data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0].map(() => Math.floor(Math.random() * 100)),
            backgroundColor: '#FFCE56'
        },
        {
            label: 'My Second Dataset',
            data: [0, 0, 0, 0, 0, 0].map(() => Math.floor(Math.random() * 100)),
            backgroundColor: '#0000FF'
        },
    ],
};



const data0 = {
    datasets: [
      {
        label: 'Dataset 1',
        data: {
            count: 10, rmin: 5, rmax: 15, min: 0, max: 100
        },
        borderColor: "#FF0000",
        backgroundColor: "#FF000000",
      },
      {
        label: 'Dataset 2',
        data: {
            count: 4, rmin: 5, rmax: 15, min: 0, max: 100
        },
        borderColor: "#FFCE56",
        backgroundColor: "#FFCE5600",
      }
    ]
  };


const data = {
    // change the color of the chart

    labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
    datasets: [
        {
            // change the color of the chart
            label: 'My First Dataset',
            data: [0, 0, 0, 0, 0, 0].map(() => Math.floor(Math.random() * 100)),
            backgroundColor: [
                '#FF6384',
                '#36A2EB',
                '#FFCE56',
                '#FF6384',
                '#36A2EB',
                '#FFCE56',
            ],
            hoverOffset: 4,
        },
    ],
};

const options = {
    plugins: {
        legend: {
            display: true,
            labels: {
                color: "#FFFFFF",
            },
            position: 'bottom',
            padding: {
                top: 20,
                bottom: 20,
                left: 20,
                right: 20
            }
        },
    },
    layout: {
        gap: 10,
    }
};
const optionsL = {
    plugins: {
        legend: {
            display: true,
            labels: {
                color: "#FFFFFF",
            },
            position: 'bottom',
            padding: {
                top: 20,
                bottom: 20,
                left: 20,
                right: 20
            },
            margin: {
                top: 20,
                
            }
        },
    },    
    scales: {
        y: {
            ticks: {
                color: "#FFFFFF",
            },
            grid: {
                color: "#FFFFFF55",
            }
        },
        x: {
            ticks: {
                color: "#FFFFFF",
            },
            grid: {
                color: "#FFFFFF55",
            }
        }
    }
            
};





function Features({section}) {
    const [cards, setCards] = useState([]);
    useEffect(() => {
        setCards([
            {
                title: "AI Assistant",
                active: true,
                metadata: {
                    feature: "Pick a character",
                    selected: "Jisa",
                }
            },
            {
                title: "Translation",
                active: true,
                metadata: {
                    feature: "Pick a language",
                    selected: "English",
                }
            },
            {
                title: "Message Builder",
                active: true,
                metadata: null
            },
            {
                title: "Tl;dr",
                active: false,
                metadata: null
            },
            {
                title: "Explain to 10 year old",
                active: true,
                metadata: null
            }, {
                title: "Magic Wheel",
                active: false,
                metadata: null
            }, {
                title: "Anti Spam",
                active: false,
                metadata: null
            }
        ]);
    }, []);
    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'row',
                alignItems: 'stretch',
                flexWrap: 'wrap',
                gap: '5px 20px',
            }}
        >
            <Wrapper>
                <Line data={data} options={optionsL} />
            </Wrapper>
            <Wrapper>
                <Pie data={data} options={options} />
            </Wrapper>
            <Wrapper>
                <Bar data={data} options={optionsL} />
            </Wrapper>
            <Wrapper>
                <Bubble data={data1} options={optionsL} />
            </Wrapper>
            <Wrapper>
                <Doughnut data={data} options={options} />
            </Wrapper>
            <Wrapper>
                <Radar data={data} options={options} />
            </Wrapper>
            <Wrapper>
                <PolarArea data={data} options={options} />
            </Wrapper>
            <Wrapper>
                <Scatter data={data} options={optionsL} />
            </Wrapper>
            <Wrapper>
                <Line data={data0} options={optionsL} />
            </Wrapper>
        </Box>
    );
}

export default Features;