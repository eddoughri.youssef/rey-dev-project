import React, { useEffect, useState } from 'react';

import BigCard from './bigCard';
import { Box } from "@mui/system";

function Servers({section}) {
    const [cards, setCards] = useState([]);
    useEffect(() => {
        setCards([
            {
                title: "Server 1",
            },
            {
                title: "Server 2",
            }
        ]);
    }, []);
    console.log(cards)
    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                gap: '5px',
            }}

        >
            {cards.map((card) => (
                <BigCard card={card}/>
            ))}
        </Box>
    );
}

export default Servers;