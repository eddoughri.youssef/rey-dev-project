import React, { useEffect } from "react";

import { Box } from "@mui/system";
import SideBar from "./sideBar";
import MainPage from "./mainPage";
import bgDash from "../../media/dashboardBackground.png";



function Dashboard() {
  const sections = [
      "Features","Analytics","Subscriptions","Servers"
  ];
  const [section, setSection] = React.useState(sections[0]);
  useEffect(() => {
    document.body.style.backgroundImage = `url(${bgDash})`;
    document.body.style.backgroundSize = "cover";
    document.body.style.backgroundRepeat = "no-repeat";
    document.body.style.backgroundPosition = "center";
    document.body.style.backgroundAttachment = "fixed";
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.backgroundImage = null;
      document.body.style.backgroundSize = null;
      document.body.style.backgroundRepeat = null;
      document.body.style.backgroundPosition = null;
      document.body.style.backgroundAttachment = null;
      document.body.style.overflow = null;
    };
  }, []);
  return (
    <Box
      sx={{
        minWidth: "100vw",
        minHeight: "100vh",
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "space-between",
        gap: "5vh",
      }}
      id="mainPage"
    >
      {/* <Container
        sx={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
        }}  
        >
            
            
      </Container> */}
      <Box
        sx={{
            // background: 'linear-gradient(180deg, #FFFFFF66, #FFFFFF09)',
            // borderRadius: '20px',
            maxHeight: '100vh',
            width: '100vw',
            // padding: '20px',
            // display: 'flex',
            display: 'grid',
            gridTemplateColumns: '300px 1px 1fr',
            gap: '20px',
            overflow: 'auto',

            position: 'relative',
        }}
      >
        <SideBar sections={sections} section={section} setSection={setSection}/>
        <Box
            sx={{
                position: 'sticky',
                top: '3px',
                bottom: '3px',
                height: '100%',
                width: '1px',
                background: 'white',
                opacity: '0.5',
            }}
        ></Box>
        <MainPage section={section}/>
      </Box>
    </Box>
  );
}

export default Dashboard;