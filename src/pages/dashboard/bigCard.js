import React from 'react';

import {
    Typography,
    Card,
    CardContent,
    Box,
    Button,
} from "@mui/material";

function BigCard({card}) {
    return (
        <Card 
            sx={{ 
                width: 320,
                backgroundColor: '#121212',
                borderRadius: '10px',
                color: '#FFFFFF',
            }}
            >
          <CardContent>
            <Box
                sx={{
                    display: 'flex',
                    alignItems: 'center',
                    gap: '10px',
                }}
            >
                <Box
                    sx={{
                        height: "55px",
                        width: "55px",
                        flex: '0 0 auto',
                        borderRadius: '100%',
                        backgroundColor: '#41F3F3',
                    }}
                >
                </Box>

                <Box
                    sx={{
                        flex: "10 1 auto",
                    }}
                >
                    <Box
                        sx={{
                            display: 'flex',
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            gap: '10px',
                        }}
                    >
                        <Typography variant="h6"
                            sx={{
                                fontSize: "1rem", color: '#FFFFFF', fontWeight: 'bold', WebkitBoxOrient: 'vertical',
                                WebkitLineClamp: 1
                            }}>{card.title}</Typography>
                    </Box>
                    <Box 
                        sx={{
                            width: '100%',
                            height: '1px',
                            backgroundColor: '#FFFFFF22',
                            margin: '10px 0%',
                        }}  
                    ></Box>
                    <Box
                        sx={{
                            display: 'flex',
                            justifyContent: 'end',
                            width: '100%',
                            minHeight: '2rem',
                            alignItems: 'center',
                        }}
                        mt={2}
                    >
                        <Button variant="outlined" sx={{
                            textTransform: 'none',
                            fontSize: '0.7rem',
                        }}>Manage Subscribtion</Button>
                    </Box>
                </Box>
            </Box>

          </CardContent>
        </Card>
      );
}

export default BigCard;