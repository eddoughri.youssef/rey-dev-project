import React, { useEffect, useState } from 'react';

import { Typography } from "@mui/material";
import Card from '@mui/material/Card';
import FormControl from '@mui/material/FormControl';
import NativeSelect from '@mui/material/NativeSelect';
import CardContent from '@mui/material/CardContent';
import Switch from '@mui/material/Switch';
import { Box } from "@mui/system";
import styled from '@mui/material/styles/styled';

function Features({section}) {
    const [cards, setCards] = useState([]);
    useEffect(() => {
        setCards([
            {
                title: "AI Assistant",
                active: true,
                metadata: {
                    feature: "Pick a character",
                    selected: "Jisa",
                }
            },
            {
                title: "Translation",
                active: true,
                metadata: {
                    feature: "Pick a language",
                    selected: "English",
                }
            },
            {
                title: "Message Builder",
                active: true,
                metadata: null
            },
            {
                title: "Tl;dr",
                active: false,
                metadata: null
            },
            {
                title: "Explain to 10 year old",
                active: true,
                metadata: null
            }, {
                title: "Magic Wheel",
                active: false,
                metadata: null
            }, {
                title: "Anti Spam",
                active: false,
                metadata: null
            }
        ]);
    }, []);
    console.log(cards)
    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'row',
                flexWrap: 'wrap',
                gap: '5px',
            }}

        >
            {cards.map((card) => (
                <CardFeature card={card}/>      
            ))}
        </Box>
    );
}

const IOSSwitch = styled((props) => (<Switch focusVisibleClassName=".Mui-focusVisible" disableRipple {...props} />))(
    ({ theme }) => ({
        width: 32,
        height: 18,
        padding: 0,
        '& .MuiSwitch-switchBase': {
        padding: 0,
        margin: 2,
        transitionDuration: '300ms',
        '&.Mui-checked': {
            transform: 'translateX(16px)',
            color: '#fff',
            '& + .MuiSwitch-track': {
            backgroundColor: theme.palette.mode === 'dark' ? '#41F3F3' : '#41F3F3',
            opacity: 1,
            border: 0,
            },
            '&.Mui-disabled + .MuiSwitch-track': {
            opacity: 0.5,
            },
        },
        '&.Mui-focusVisible .MuiSwitch-thumb': {
            color: '#33cf4d',
            border: '6px solid #fff',
        },
        '&.Mui-disabled .MuiSwitch-thumb': {
            color:
            theme.palette.mode === 'light'
                ? theme.palette.grey[100]
                : theme.palette.grey[600],
        },
        '&.Mui-disabled + .MuiSwitch-track': {
            opacity: theme.palette.mode === 'light' ? 0.7 : 0.3,
        },
        },
        '& .MuiSwitch-thumb': {
        boxSizing: 'border-box',
        width: 12,
        height: 15,
        },
        '& .MuiSwitch-track': {
        borderRadius: 26 / 2,
        backgroundColor: theme.palette.mode === 'light' ? '#E9E9EA' : '#39393D',
        opacity: 1,
        transition: theme.transitions.create(['background-color'], {
            duration: 500,
        }),
        },
    }));

function CardFeature({card}) {
  return (
    <Card 
        sx={{ 
            width: 300,
            backgroundColor: '#121212',
            borderRadius: '10px',
            color: '#FFFFFF',
        }}
        >
      <CardContent>
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                gap: '10px',
            }}
        >
            <Typography variant="h6"
                sx={{
                    fontSize: "1rem", color: '#FFFFFF', fontWeight: 'bold', WebkitBoxOrient: 'vertical',
                    WebkitLineClamp: 1
                }}>{card.title}</Typography>
            <IOSSwitch
                defaultChecked={card.active}
                inputProps={{ 'aria-label': 'controlled' }}
            />
        </Box>
        <Box 
            sx={{
                width: '100%',
                height: '1px',
                backgroundColor: '#FFFFFF22',
                margin: '5px 0%',
            }}  
        >

        </Box>
        <Box
            sx={{
                display: 'flex',
                justifyContent: 'space-around',
                width: '100%',
                minHeight: '2rem',
                alignItems: 'center',
            }}
        >
            {card.metadata && (
                <>
                    <Typography variant="body2" sx={{color: '#FFFFFF'}}>{card.metadata ? card.metadata.feature : null}</Typography>
                    <FormControl>
                        <NativeSelect
                            defaultValue={30}
                            inputProps={{
                                name: 'age',
                                id: 'uncontrolled-native',
                            }}
                            sx={{
                                color: '#FFFFFF',
                                backgroundColor: '#121212',
                            }}
                        >
                        <option value={10}>Option 1</option>
                        <option value={20}>Option 1</option>
                        <option value={30}>Option 1</option>
                        </NativeSelect>
                    </FormControl>
                </>)
            }
        </Box>
      </CardContent>
    </Card>
  );
}

export default Features;