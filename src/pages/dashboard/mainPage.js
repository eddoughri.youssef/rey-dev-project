import React from 'react';

import { Typography } from "@mui/material";
import { Box } from "@mui/system";



import Placeholder from './placeholder';
import Features from './features';
import Subscription from './subscription';
import Analytics from './analytics';

export default function mainPage({ section, setSection}) {
    const sections = {
        "Features": <Features section={section} />,
        "Analytics": <Analytics section={section} />,
        "Subscriptions": <Subscription section={section} />,
        "Servers": <Placeholder section={section} />,
    }
    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                padding: '50px 20px 0px 30px',
                gap: '25px',
            }}
        >
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    marginBottom: '20px',
                }}
            >
                <Typography variant="h3" sx={{color: '#FFFFFF', fontWeight: 'bold'}}>{section}</Typography>
                <Box
                    sx={{
                        height: '70px',
                        width: '70px',
                        background: '#41F3F3',
                        boxShadow: '0px 4px 50px 0px #41ABF320',
                        borderRadius: '100%',
                    }}
                >
                </Box>    
            </Box>
            {sections[section]}
        </Box>
  )
}