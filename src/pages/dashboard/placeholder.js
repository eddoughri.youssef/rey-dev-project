import React from 'react';

import { Typography } from "@mui/material";
import { Box } from "@mui/system";

function placeholder({section}) {
    return (
        <Box>
            <Typography variant="h5" sx={{color: '#FFFFFF', fontWeight: 'bold'}}>Welcome to Jisa</Typography>
            <Typography variant="h5" sx={{color: '#FFFFFF', fontWeight: 'bold'}}>Your {section} are empty</Typography>
        </Box>
    );
}

export default placeholder;