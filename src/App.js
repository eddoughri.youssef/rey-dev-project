import React from "react";

import { Route, Routes } from "react-router-dom";
import { useState } from 'react';


import Skeleton from "./skeleton";
import MainPage from "./pages/mainPage";
import ReactPlayer from "react-player";


import Payment from "./pages/payment";
import Dashboard from "./pages/dashboard";
// import PaymentForm from "./pages/payment/authentificate/PaymentForm";
// import PurchaseForm from "./component/purchase/PurchaseForm";
// import PlanForm from "./pages/payment/plan/PlanForm";

function App() {
  const [showPlayer, setShowPlayer] = useState({play: false, code: null});
  const updateTheYtbCode = (code)=>{
    const obj={
      play: true,
      ytb_code: code
    }
    setShowPlayer(obj);
  }
  const hideYtbVideo = ()=>{
    const obj={
      play: false,
      ytb_code: null
    }
    setShowPlayer(obj);
  }
  return (
    <>
      <Routes>
        <Route element={<Skeleton />}>
          <Route path="/" element={<MainPage updateTheState={updateTheYtbCode} showPlayer={showPlayer}/>} />
          <Route path="/payment" element={<Payment />} />
          <Route path="/dashboard" element={<Dashboard />}/>
        </Route>
      </Routes>
      {
          showPlayer.play && 
          <div className='fixed top-0 h-screen w-screen z-[1000] font-poppins text-white flex justify-center items-center'
            onClick={()=>{
              console.log("hide the youtube video player");
              hideYtbVideo();
            }}
          >
            <ReactPlayer url={`https://www.youtube.com/watch?v=${showPlayer.ytb_code}`} playing={true} pip={false} width='70%' height='70%'/>
          </div>
      }
    </>
  );
}

export default App;
