
import { getPlans } from "./handlePlans";

export async function getSubscription(id){
    let subscriptionJson = null;
    try{
        const subscriptionResponse = await fetch(`http://localhost:5000/subscription?subscribtionId=${id}`, {
            method: "GET",
        });
        subscriptionJson = await subscriptionResponse.json();
    }catch(err){
        console.log(err);
        return null;
    }
    return subscriptionJson;
}

export async function updateSubscription(planid,id){
    let subscriptionJson = null;
    try{
        const subscriptionResponse = await fetch(`http://localhost:5000/subscription/update?subscribtionId=${id}&PlanId=${planid}`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            // body: JSON.stringify(payload),
        });
        subscriptionJson = await subscriptionResponse.json();
    }catch(err){
        console.log(err);
        return null;
    }
    return subscriptionJson;
}

export async function createSubscription(discordId, plan) {
    const subscribtion = await fetch(`http://localhost:5000/subscription/create/payment-intent?plan=${plan}&discordId=${discordId}`,{ method: "GET" });
    const subscribtionJson = await subscribtion.json();
    return subscribtionJson;
}
