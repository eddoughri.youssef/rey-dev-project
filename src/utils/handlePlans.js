export async function getPlans(id){
    let planJson = null;
    try{
        const planResponse = await fetch(`http://localhost:5000/plan?planId=${id}`, {
            method: "GET",
        }); // get the plan with the id from the database
        planJson = await planResponse.json();
    }catch(err){
        console.log(err);
        return null;
    }
    return planJson;
}

export async function searchPlans(id){
    let productJson = null;
    try{
        const planResponse = await fetch(`http://localhost:5000/plan/search?product=${id}`, {
            method: "GET",
        });
        productJson = await planResponse.json();
    }catch(err){
        console.log(err);
        return null;
    }
    return productJson;
}

export async function createPlan(plan){
    // plan should have { name, description, servers, botname }
    let newPlanJson = null;
    try{
        const planResponse = await fetch(`http://localhost:5000/plan/create`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(plan),
        });
        newPlanJson = await planResponse.json();
    }catch(err){
        console.log(err);
        return null;
    }
    return newPlanJson;
}

export async function updatePlan(plan){
    // plan should have { planId, name, description, servers, botname, currentPlanId }
    let newPlanJson = null;
    try{
        const planResponse = await fetch(`http://localhost:5000/plan/update`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify(plan),
        });
        newPlanJson = await planResponse.json();
    }catch(err){
        console.log("reached updatePlan")
        console.log(err);
        return null;
    }
    return newPlanJson;
}