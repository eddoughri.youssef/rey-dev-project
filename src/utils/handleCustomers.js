


export async function getCustomer(id){
    let customerJson = null;
    try{
        const customerResponse = await fetch(`http://localhost:5000/customer?discordId=${id}`, {
            method: "GET",
        });
        customerJson = await customerResponse.json();
    }catch(err){
        console.log(err);
        return null;
    }
    return customerJson;
}

export async function createCustomer(customer){
    let customerJson = null;
    try{
        const customerResponse = await fetch(`http://localhost:5000/customer`, {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify(customer)
        });
        customerJson = await customerResponse.json();
        if(customerJson.status === "ERROR"){
            return null;
        }
    }catch(err){
        console.log(err);
        return null;
    }
    return customerJson;
}