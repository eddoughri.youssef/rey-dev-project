const getFromDiscordApi = async (baseUrl ,handler ,method ,body ,access_token) => {
    const response = {};
    const data = {
        method: method,
        headers: {
            "Content-Type": "application/x-www-form-urlencoded",
        },
    };
    if(access_token) data.headers.Authorization = `Bearer ${access_token}`;
    if(body) data.body = body;
    try{
        const res = await fetch(baseUrl, data);
        if(res.status === 200){
            response.status = "success";
            const result = await res.json();
            response.data = handler(result);
            
        }
        else if(res.status >= 300){
            response.status = "failed";
            response.message = "Request failed";
        }else{
            console.log("UNHANDLE RESPONSE", res?.status);
        }
    }catch(err){
        response.status = "failed";
        response.message = err;
    }
    return new Promise((resolve, reject) => {
        if(response.status === "success"){
            resolve(response);
        }
        else{
            reject(response);
        }
    });
};

const getTheToken = async (grantCode) => {
    const client_id = process.env.REACT_APP_DISCORD_CLIENT_ID;
    const client_secret = process.env.REACT_APP_DISCORD_CLIENT_SECRET;
    const grant_type = "authorization_code";
    const code = grantCode;
    const redirect_uri = process.env.REACT_APP_DISCORD_REDIRECT_URI;
    const scope = process.env.REACT_APP_DISCORD_SCOPE.split(" ").join("%20");
    const body = `client_id=${client_id}&client_secret=${client_secret}&grant_type=${grant_type}&code=${code}&redirect_uri=${redirect_uri}&scope=${scope}`;
    const baseUrl = `https://discord.com/api/oauth2/token`;
    const method =  "POST";
    const handler = (data) => {
        const {access_token, token_type, expires_in, refresh_token, scope} = data;
        localStorage.setItem("access_token", access_token);
        localStorage.setItem("token_type", token_type);
        localStorage.setItem("expires_in", expires_in);
        localStorage.setItem("refresh_token", refresh_token);
        localStorage.setItem("scope", scope);
        return data;
    };
    try{
        const response = await getFromDiscordApi(baseUrl, handler, method, body);
        console.log("token has been fetched successfully");
        console.log(response);
        return true;
    }catch(err){
        console.log("failed to get the token");
        console.log(err);
        return false;
    }
};
  
const getUserInfo = async () => {
    const access_token = localStorage.getItem("access_token");
    const baseUrl = `https://discord.com/api/users/@me`;
    const method = "GET";
    const handler = (data) => data;
    try{
        const response = await getFromDiscordApi(baseUrl, handler, method, null, access_token);
        console.log("the user info is ", response);
        return response.data;
    }catch(err){
        console.log("failed to fetch user info");
        console.log(err);
        return false;
    }
}    
  
const getAllOwnedServers = async () => {
    const access_token = localStorage.getItem("access_token");
    const baseUrl = `https://discord.com/api/users/@me/guilds`;
    const method = "GET";
    const handler = (data) => data.filter((server) => server.owner);
    try{
        const response = await getFromDiscordApi(baseUrl, handler, method, null, access_token);
        console.log("the servers are", response);
        return response.data;
    }catch(err){
        console.log("failed to fetch user info");
        console.log(err);
        return false;
    }
}




export {getTheToken, getUserInfo, getAllOwnedServers};
