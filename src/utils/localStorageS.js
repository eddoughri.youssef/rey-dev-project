export function deleteItemAfter(key, time, callback){
  setTimeout(() => {
    callback();
    localStorage.removeItem(key);
  }, time);
}