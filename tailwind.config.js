/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ["./src/**/*.{js,jsx,ts,tsx}"],
    theme: {
      extend: {
        colors: {
          bg: {
            blue: "#2A1968",
            skyBlue: "#41ABF3",
            gray: "#F0F0F0",
            red: "#C84B4B",
          }
        },
        fontFamily: {
          poppins: ['Poppins', 'sans-serif'],
          bebasNeue: ['Bebas Neue', 'cursive'],
          syncopate: ['Syncopate', 'sans-serif'],
        },
        fontSize: {
          'header': ['55px', {
            lineHeight: '75px',
          }],
          'subheader': ['45px', {
            lineHeight: '75px',
          }],
          'paragraph': ['14px', {
            lineHeight: '22px',
          }],
          '8xl': ['6rem', {
            lineHeight: '85px',
          }],
        },
        dropShadow: {
          'skyblue': '0 0 12px rgba(65, 171, 243, 0.25)',
          'blue': '0 0 12px rgba(42, 25, 104, 0.2)',
        }
      },
    },
    plugins: [],
  }
  